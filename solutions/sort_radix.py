"""
How do you implement a radix sort in python?

Unlike comparison sorting algorithms like bubble or merge sorting, radix works by sorting numbers
based on their least significant digit.

    292992 - 2
         5  - 5
    102928  - 8

And then we proceed for the next least significant digit.
"""


def sort_radix(l: list) -> list:
    """
    Implementation of a radix sorting algorithm without using a separate counting method.
    >>> sort_radix([19821, 14, 5, 100, 307, 1, 68])
    [1, 5, 14, 68, 100, 307, 19821]
    """
    RADIX: int = 10
    max_length: bool = False
    tmp: int = -1
    placement: int = 1

    while not max_length:
        max_length = True
        # declare and initialize buckets
        buckets: list = [list() for _ in range(RADIX)]

        # split l between lists
        for i in l:
            tmp = i // placement
            buckets[tmp % RADIX].append(i)  # buckets of 1, then buckets of 10, 100s, 1000s etc
            if max_length and tmp > 0:
                max_length = False

        # empty lists into l array
        a: int = 0
        for b in range(RADIX):
            bucket = buckets[b]
            i: int = 0
            for i in bucket:
                l[a] = i
                a += 1

        # move to next digit
        placement *= RADIX

    return l
