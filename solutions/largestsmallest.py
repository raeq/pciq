"""
How do you find the largest and smallest number in an unsorted integer list?

Use the min and max functions on the list.

"""


def largest_smallest(numbers: list)-> list:
    """
    >>> largest_smallest([7, 1, 5, 20, 8])
    [1, 20]

    :param numbers:
    :return:
    """
    answer: list = list()
    answer.append(min(numbers))
    answer.append(max(numbers))
    return answer
