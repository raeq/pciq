"""
How do you count a number of vowels and consonants in a given string?
Well, this question has some not so subtle subtleties. For example, we're not being asked to count whitespace, digits,
or punctuation marks.
Typically, regular expressions can be used to do this very efficiently - but there are, of course, many other
ways to do this in Python.
"""


def count_vowels_consonants_regex(sentence: str) -> tuple:
    """
    The following test sentence has 36 letter characters, of which 12 are vowels and 24 are consonants.
    We will use two regular expressions, one to find the vowels, and one to find all letter characters - including
    vowels. We'll add the optional flags for case insensitive and multiline searches.

    >>> count_vowels_consonants_regex('The quIck brown fox jumped over the lazy dog.')
    (12, 24)
    """

    import re

    letter_pattern = re.compile(r"[a-zA-Z]", re.IGNORECASE or re.MULTILINE)
    letters: str = ''.join(letter_pattern.findall(sentence))
    count_l: int = len(letters)

    # other languages may have additional vowels, for instance with accents or umlauts.
    vowel_pattern = re.compile(r"[aeiou]", re.IGNORECASE or re.MULTILINE)
    count_v: int = len(vowel_pattern.findall(letters))

    return count_v, count_l - count_v


def count_vowels_consonants_lambda(sentence: str) -> tuple:
    """
    This is a simpler way, not requiring the use of regex.
    The lambda is used to specify the given characters from the input sentence.
    Filter applies the lambda to the input sentence.
    We need to turn the filter's output into a list and then derive its length.

    >>> count_vowels_consonants_lambda('The quIck brown fox jumped over the lazy dog.')
    (12, 24)
    """

    count_v: int = len(list(filter(lambda ch: ch.lower() in "aeiou", sentence.lower())))
    count_c: int = len(list(filter(lambda ch: ch.lower() in "bcdfghjklmnpqrstvwxyz", sentence.lower())))

    return count_v, count_c


def count_vowels_consonants_sets(sentence: str) -> tuple:
    """
    We can also use more traditional iteration to count manually.

    >>> count_vowels_consonants_sets('The quIck brown fox jumped over the lazy dog.')
    (12, 24)
    """

    vowels: set = set('aeiou')
    consonants: set = set('bcdfghjklmnpqrstvwxyz')

    count_v: int = 0
    count_c: int = 0

    for char in sentence.lower():
        if char in consonants:  # consonants are more frequent, so we'll test this first
            count_c += 1
        elif char in vowels:
            count_v += 1

    return count_v, count_c
