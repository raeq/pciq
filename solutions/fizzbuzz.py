def fizzbuzz():
    """
    Write a program that prints the numbers from 1 to 100. But for multiples of three print "Fizz" instead of the
    number and for the multiples of five print "Buzz". For numbers which are multiples of both three and five
    print "FizzBuzz".

    https://blog.codinghorror.com/why-cant-programmers-program/

    The trick here lies in the wording. Every number must print something, either the number, Fizz, Buzz, or FizzBuzz.
    >>> fizzbuzz()
    1
    2
    Fizz
    4
    Buzz
    Fizz
    7
    8
    Fizz
    Buzz
    11
    Fizz
    13
    14
    FizzBuzz
    ...
    """
    for val in range(1, 101):
        ret = val  # prints the numbers from 1 to 100

        if val % (3 * 5) == 0:  # multiples of both three and five print "FizzBuzz"
            ret = "FizzBuzz"
        elif val % 3 == 0:  # multiples of three print "Fizz
            ret = "Fizz"
        elif val % 5 == 0:  # multiples of five print "Buzz"
            ret = "Buzz"

        if ret is not None:
            print(ret)
