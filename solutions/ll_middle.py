"""
How to find the center element in a linked list in one pass??
This code uses the Node class found in the module .linkedlist.py
"""

from . import linkedlist as ll


def middle_linked_list_inefficent():
    """
    This is not space efficient, since we essentially copy the entirety of the linked list objects into
    a list structure, then return the middle. If we wanted a list object then we wouldn't be using linked lists
    in the first place.
    >>> middle_linked_list_inefficent()
    second
    """
    n: ll.Node = ll.Node("first")
    n.next = ll.Node("second")
    n.next.next = ll.Node("third")

    llist: list = list()

    while n:
        llist.append(n)
        n = n.next

    print(llist[len(llist) // 2])


def middle_linked_list_efficent():
    """
    This is more efficient.
    We traverse the linked list, but we keep a tick-tock to store every other node.
    Because we're only storing evert other node, the tick-tock is moving half as fast.
    >>> middle_linked_list_efficent()
    second
    """
    n: ll.Node = ll.Node("first")
    n.next = ll.Node("second")
    n.next.next = ll.Node("third")

    ticktock: str = 'tick'
    temp_node: ll.Node = None

    while n:
        if ticktock is 'tock':
            temp_node = n
            ticktock = 'tick'
        else:
            ticktock = 'tock'
        n = n.next

    print(temp_node)
