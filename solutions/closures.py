""""
Python supports both closures and nested functions. They look similar, but are not.

Closures in python are defined in the following way:

    * We must have a nested function (function inside a function).
    * The nested function must refer to a value defined in the enclosing function.
    * The enclosing function must return the nested function.

"""


def outer(msg: str) -> str:
    """
    >>> closure1 = outer("foo")
    >>> print (closure1("bar"))
    foobar
    >>> closure1 = outer("bar")
    >>> print (closure1("foo"))
    barfoo
    """

    # This is a nested function
    def closure(innermsg) -> str:
        return msg + innermsg  # we refer to a value defined in the enclosing function

    return closure  # we return the enclosing function


def firstlast(msg: str) -> str:
    """
    Same principle applies. We assign a closure to a variable. Remember, closures return a function.
    Then we call the closure with our value.
    >>> myfunc = firstlast ("foo")
    >>> myfunc ("bar")
    'fr'
    >>> myfunc ("baz")
    'fz'
    """

    def inner(outermsg) -> str:
        return msg[:1] + outermsg[-1:]  # first letter plus last letter

    return inner


def outer3(x):
    """
    We can provide arguments to inner closure cells from
    an external caller.
    >>> outer3('Brett')('likes')('beer.')
    'Brett likes beer.'

    We can alsoo assign the closure to a variable, nd supply the last value as we see fit.
    >>> myfunc = outer3('Brett')('likes')
    >>> myfunc('chips.')
    'Brett likes chips.'
    """

    def intermediate(y):
        def inner(z):
            return '%s %s %s' % (x, y, z)

        return inner

    return intermediate


def outer4(value: str):
    """
    Within an enclosed function, trying to modify any variable in an outer scope
    will lead to an exception.
    >>> myfunc = outer4("Test")
    >>> myfunc("Error")
    Traceback (most recent call last):
        ...
    UnboundLocalError: local variable 'someval' referenced before assignment
    """
    someval = "B"

    def inner(msg):
        someval += "reak"
        return value + msg + someval

    return inner


def outer5(value: str):
    """
    We can fix this error by adding the nonlocal keyword.
    >>> myfunc = outer5("Test")
    >>> myfunc("Error")
    'TestErrorBreak'
    """
    someval = "B"

    def inner(msg):
        nonlocal someval  # add the nonlocal keyword to copy the outer variable to the inner scope
        someval += "reak"
        return value + msg + someval

    return inner
