"""
Using WordNet from princeton, we can find the synonyms of a word.
"""

from nltk.corpus import wordnet as wn


def synonyms(word: str) -> list:
    """
    Let's use WordNet in NLTK to find the synsets of the given word.
    >>> synonyms('fast')
    ['debauched', 'degenerate', 'degraded', 'dissipated', 'dissolute', 'fast', 'fasting', 'firm', 'flying', 'immobile', 'libertine', 'loyal', 'profligate', 'quick', 'riotous', 'tight', 'truehearted']
    >>> synonyms('small')
    ['belittled', 'diminished', 'humble', 'little', 'low', 'lowly', 'minor', 'minuscule', 'modest', 'pocket-size', 'pocket-sized', 'small', 'small-scale']
    >>> synonyms('slip')
    ['berth', 'case', 'chemise', 'cutting', 'dislocate', 'drop_away', 'drop_off', 'eluding', 'elusion', 'err', 'fall_away', 'faux_pas', 'gaffe', 'gaucherie', 'luxate', 'miscue', 'mistake', 'moorage', 'mooring', 'parapraxis', 'pillow_slip', 'pillowcase', 'shift', 'shimmy', 'sideslip', 'skid', 'slew', 'slick', 'slickness', 'slide', 'slip', 'slip-up', 'slip_of_paper', "slip_one's_mind", 'slipperiness', 'slue', 'sneak', 'solecism', 'splay', 'steal', 'strip', 'teddy', 'trip']

    """

    lemmas = set()
    for ss in wn.synsets(word):
        for n in ss.lemma_names():
            lemmas.add(n)

    return sorted(list(lemmas))
