"""

How do you implment a system which outputs changes to the console every time a class instance property changes? ([])()
We'll implement the design pattern "Observer" on a simple 'product' class.
"""


class Product:
    __slots__ = ['_name', '_id', '_price', 'observers']

    def __init__(self, product_name: str, product_id: str, price: float):
        """ Object initializer """
        self.observers = []
        self.name = product_name
        self.id = product_id
        self.price = price

    def attach(self, observer):
        """ Add a new observer """
        self.observers.append(observer)

    def _notify_observers(self):
        for observer in self.observers:
            observer()

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value: str):
        self._name = value
        self._notify_observers()

    @property
    def id(self) -> str:
        return self._id

    @id.setter
    def id(self, value: str):
        self._id = value
        self._notify_observers()

    @property
    def price(self) -> float:
        return self._price

    @price.setter
    def price(self, value: float):
        self._price = value
        self._notify_observers()

    def __repr__(self):
        return f"ID: {self.id}, Name: {self.name}, Price: {self.price}"


class ConsoleObserver:
    def __init__(self, observed):
        self.observed = observed
        observed.attach(self)

    def __call__(self):
        print(self.observed.__repr__())


def driver():
    """

    :return:
    >>> driver()
    ID: 001_FNC, Name: Small FNC, Price: 24.9
    ID: 001_FNC, Name: Steak Pie, Price: 24.9
    ID: 002_STP, Name: Steak Pie, Price: 24.9
    ID: 002_STP, Name: Steak Pie, Price: 32.6
    """
    p = Product("Small FNC", "001_FNC", 12.8)
    c = ConsoleObserver(p)
    p.price = 24.9
    p.name = 'Steak Pie'
    p.id = '002_STP'
    p.price = 32.6
