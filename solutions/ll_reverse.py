"""
How to reverse a linked list?
This code uses the Node class found in the module .linkedlist.py
"""

from . import linkedlist as ll


def reverse_linked_list_iteration():
    """
    This method walks through the linked list from beginning to end.
    We instantiate a new Node for each item, and add it to Node.previous in order to reverse the list.
    This uses no temporary variables, but is a bit ugly.
    >>> reverse_linked_list_iteration()
    third
    second
    first
    """
    n: ll.Node = ll.Node("first")
    n.next = ll.Node("second")
    n.next.next = ll.Node("third")

    p: ll.Node = ll.Node(n.payload)

    while n:
        n = n.next
        if n is not None:
            p.previous = ll.Node(n.payload)
            p = p.previous

    while p:
        print(p)
        p = p.next
