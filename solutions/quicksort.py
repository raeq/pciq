def quickSort(numbers):
    """
    >>> quickSort([2,4,3,5,1])
    [1, 2, 3, 4, 5]

    :param numbers:
    :return:
    """

    def quickSortHelper(numbers, first, last):

        def partition(numbers, first, last):
            pivotvalue = numbers[first]

            leftmark = first + 1
            rightmark = last

            done = False
            while not done:

                while leftmark <= rightmark and numbers[leftmark] <= pivotvalue:
                    leftmark = leftmark + 1

                while numbers[rightmark] >= pivotvalue and rightmark >= leftmark:
                    rightmark = rightmark - 1

                if rightmark < leftmark:
                    done = True
                else:
                    temp = numbers[leftmark]
                    numbers[leftmark] = numbers[rightmark]
                    numbers[rightmark] = temp

            temp = numbers[first]
            numbers[first] = numbers[rightmark]
            numbers[rightmark] = temp

            return rightmark
        
        if first < last:
            splitpoint = partition(numbers, first, last)
            quickSortHelper(numbers, first, splitpoint - 1)
            quickSortHelper(numbers, splitpoint + 1, last)

        return numbers

    return quickSortHelper(numbers, 0, len(numbers) - 1)