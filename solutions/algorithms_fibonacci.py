"""
The Fibonacci series of number is a popular interview question. Like FizzBuzz, it is full of pitfalls for a
seemingly simple tasks. The series is defined as follows: every number after the first two is the sum of the two
preceding ones:

    1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144

https://en.wikipedia.org/wiki/Fibonacci_number

We'll describe several ways to do this!

"""


def fibonacci_recursive(i: int = None) -> int:
    """
    The simplest way to calculate a fibonacci number, using recursion.
    Simple works, but not in all cases!

    >>> fibonacci_recursive(10)
    55

    >>> assert fibonacci_recursive(10) == F(10)

    >>> assert fibonacci_recursive(25) == F(25)

    >>> fibonacci_recursive(30)
    832040

    >>> # fibonacci_recursive(100) No, don't do this, it will take forever and exhaust your computer's memory
    """
    if i == 0:
        return 0
    if i == 1:
        return 1
    return fibonacci_recursive(i - 1) + fibonacci_recursive(i - 2)


def fibonacci_bottomup(target: int = None) -> int:
    """
    This technique is the most common. We transpose the previous solution. Instead of recursively
    calculating all possible previous numbers, why not iterate from the bottom up?

    :param i:
    :return:

    >>> fibonacci_bottomup(10)
    55

    >>> fibonacci_bottomup(50)
    12586269025

    >>> fibonacci_bottomup(80)
    23416728348467685

    >>> assert fibonacci_bottomup(80) == F(80)

    >>> fibonacci_bottomup(100)
    354224848179261915075

    >>> fibonacci_bottomup(1000)
    43466557686937456435688527675040625802564660517371780402481729089536555417949051890403879840079255169295922593080322634775209689623239873322471161642996440906533187938298969649928516003704476137795166849228875

    """
    smallerresults = [0, 1]  # a nice list, containing all previously calculated fibonacci numbers
    for i in range(2, target + 1):
        smallerresults.append(smallerresults[-1] + smallerresults[-2])
    return int(smallerresults[-1::].pop())  # lets get the last value calculated, it's our target


def fibonacci_iterative(target: int) -> int:
    """
    This version uses iteration to start at the beginning of the sequence and work up to the target value.
    It is in this way similar to fibonacci_bottomup
    Luckily, the iterative version doesn't have the memory space issues, there are no deeply nested call chains here.
    :param target:
    :return:


    >>> fibonacci_iterative(10)
    55

    >>> fibonacci_iterative(40)
    102334155

    >>> fibonacci_iterative(50)
    12586269025

    >>> fibonacci_iterative(80)
    23416728348467685

    >>> assert fibonacci_iterative(80) == F(80)

    >>> fibonacci_iterative(1000)
    43466557686937456435688527675040625802564660517371780402481729089536555417949051890403879840079255169295922593080322634775209689623239873322471161642996440906533187938298969649928516003704476137795166849228875
    """
    penultimate, ultimate = 0, 1  # the values at target -2 and target -1
    for i in range(target):
        penultimate, ultimate = ultimate, penultimate + ultimate  # simple: just calculate the new values
    return penultimate


def memo(func):
    """
    An @memo decorator for functions and methods.
    It defines an internal cache. The key is value of the parameter used in the wrapped function.
    Say we call a fibonacci generator with a value of 30. The result is stored in the cache:

        cache[30] = 1346269

    When we call the fibonacci generator with 30, we can just get the value for 30 out of the cache instead
    of calculating it all again. Pretty goddamn sweet.

    Python has an inbuilt decorator which does exactly this, but nevertheless this code is instructive.
    The decorator is:
    @functools.lru_cache(maxsize=None)

    """
    from functools import wraps  # let's make a decorator!
    cache = {}  # this is a cache to store intermediate results :)

    @wraps(func)  # use a decorator on our wrapper to preserve nice things
    def wrap(*args, **kwargs):
        if args not in cache:  # if we've already calculated this value, put it in the cache
            cache[args] = func(*args, **kwargs)  # call the original function
        return int(cache[args])  # always return the value from the cache

    return wrap


@memo
def fibonacci_wrapped(i: int = None) -> int:
    """
    This technique uses exactly the same recursive algorithm as in the naive solution.
    However, we are now using a technique known as "memoization". This is really useful in some
    recursive algorithms, because otherwise the same set of values is constantly being calculated.
    Here we use a nice and simple decorator to add the memoization capability.

    However, we're still using recursion, and at some point the maximum recursion depth will be reached.

    The technique of memoization is pretty sweet! We can memoize an existing recursive function to make
    it perform much better.


    >>> assert fibonacci_wrapped(10) == F(10)
    >>> assert fibonacci_wrapped(10) == fibonacci_recursive(10)

    >>> fibonacci_wrapped(40)
    102334155

    >>> fibonacci_wrapped(50)
    12586269025

    >>> fibonacci_wrapped(80)
    23416728348467685

    >>> assert fibonacci_wrapped(80) == F(80)

    >>> fibonacci_wrapped(100000)
    Traceback (most recent call last):
    ...
    RecursionError: maximum recursion depth exceeded...
    """
    if i == 0:
        return 0
    if i == 1:
        return 1
    return int(fibonacci_wrapped(i - 1) + fibonacci_wrapped(i - 2))


def F(target: int) -> int:
    """
    This is the fastest way to compute a fibonacci number, using matrix mathematics.
    It completes quickly.
    The math is explained in detailed on the Wiki page at: https://en.wikipedia.org/wiki/Fibonacci_number#Matrix_form

    >>> F(10)
    55

    >>> F(40)
    102334155

    >>> F(80)
    23416728348467685

    :param n:
    :return:
    """
    v1, v2, v3 = 1, 1, 0  # initialise a matrix [[1,1],[1,0]] of the first two numbers in the sequence

    for record in bin(target)[3:]:  # perform fast exponentiation of the matrix (quickly raise it to the nth power)
        calc = v2 * v2
        v1, v2 = v1 * v1 + calc, (v1 + v3) * v2
        v3 = calc + v3 * v3

        if record is '1':
            v1, v2, v3 = v1 + v2, v1, v2
    return int(v2)


def fibonacci_goldenratio(target):
    """
    This method works by calculating the golden ratio, and then
    raising the golden ration to the power of n, whereby n is the position of the fibonacci sequence.

    As we look for higher and higher values of n, rounding errors throw us off. This method works for values of n
    less than 72 or so.

    :param target:
    :return:

    >>> fibonacci_goldenratio(40)
    102334155

    >>> assert fibonacci_goldenratio(43) == F(43)
    >>> assert fibonacci_goldenratio(71) == F(71)
    """
    import math

    if target > 71:
        return F(target)

    sqrt5 = math.sqrt(5)
    golden_ratio = (1 + sqrt5) / 2

    return math.floor(math.pow(golden_ratio, target) / sqrt5)
