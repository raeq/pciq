""""
How do you check if a given string is a palindrome of another? Palindromes say the say thing spelled forward as back.
So what is a plaindrome? Well, there are different kinds of course, the most famous being character-level
palindromes (individual words) which say the same thing spelled forwards and backwards:

    radar
    civic
    madam
    refer
"""


def is_palindromic(word: str) -> bool:
    """
    Checking character-unit palindromes is easy, we just use Python's in-built reverse slice method [::-1].

    >>> is_palindromic('civic')
    True
    >>> is_palindromic('madam')
    True
    >>> is_palindromic('radar')
    True
    >>> is_palindromic('refer')
    True
    >>> is_palindromic('friendship')
    False
    """

    if word[::-1] == word:
        return True
    else:
        return False
