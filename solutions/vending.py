"""
How do you design a vending machine?

Obviously, there are a lot of different possible ways to do this.
When modelling real - world objects, I like to use object oriented programming.
"""


class Product(object):
    """
    Implements the vending machine's product class.
    >>> p: Product = Product('donuts', 32)
    >>> print(p)
    Product: n:donuts p:32
    """

    __slots__ = ['name', 'price']

    def __init__(self, name: str, price: float):
        self.name = name
        self.price = price

    def __repr__(self):
        return f'Product: n:{self.name} p:{self.price}'

    def __eq__(self, other):
        """
        >>> r: Product = Product('a', 10)
        >>> l: Product = Product('a', 10)
        >>> r == l
        True
        """
        if isinstance(other, self.__class__):
            return self.__repr__() == other.__repr__()
        return False

    def __ne__(self, other):
        """
        >>> r: Product = Product('a', 10)
        >>> l: Product = Product('b', 20)
        >>> r != l
        True
        >>> r != 10
        True
        """
        if isinstance(other, self.__class__):
            return self.__repr__() != other.__repr__()
        return True


class VendingMachine(object):

    def __init__(self):
        pass

    def add_product(self, p: Product, q: int):
        pass

    def count_coins(self, coins: float):
        pass

    def return_change(self):
        pass

    def dispense_product(self):
        pass

    def user_selection(self, selection: int):
        pass


if __name__ == "__main__":
    import doctest

    doctest.testmod()
