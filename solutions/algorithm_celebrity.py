"""
If you're asked a question of the type:

    In a crowded restaurant, everybody knows one member who is a celebrity.
    But the celebrity is dining alone, and doesn't know anybody.
    Find the person who is the celebrity.

The question can be asked differently, such as find the thread that over threads are waiting on etc. The real point is
that, again, we're talking about a graph structure. Every vertice (node) but one has an edge (connection) directed
towards our celebrity. But the celebrity has no outward connections.

Find the celebrity.

"""


def get_random_graph(vertices: int) -> list:
    """
    Let's first define a helper function to generate a graph with n vertices and some edges.

    OK, this is tricky.
    randrange(2) gives a value between 0 and 1. Either on or off.
    The length parameter tells us how many rows - the same number of columns is used.

    If called with a length value of 3, something like this is returned (but random 0s and 1s).

    # >>> print (get_random_graph(3))
    [[0, 1, 1], [1, 1, 1], [0, 0, 1]]       # example random output

    This is a graph of three nodes.
    The first node has two edges, pointing to nodes 2 and 3 [0, 1, 1]
    The second node has three edges, pointing to nodes 1, 2 and 3 [1, 1, 1]
    The third node has one edge, pointing to itself  [0, 0, 1]. And why not?


    :param vertices: how big your 0 or 1 graph should be
    :return: the list of Ohs or Ones

    """
    from random import randrange
    return [[randrange(2) for i in range(vertices)] for i in range(vertices)]


def make_celebrity_in_crowd(crowd: list) -> list:
    """
    Given a grid of zeros or ones, we'll select a lucky person to be the celebrity.

    :param crowd: A
    :return: Returns a graph. One of the elements has no directed edges outwards.

    #>>> crowd = get_random_graph(3)
    #>>> print (make_celebrity_in_crowd(get_random_graph(3)))

    This example output shows that item 3 (the third vertice) was selected to know no one else.

    [[0, 1, True], [1, 1, True], [False, False, False]]

    Each of the vertices has True in position 3, pointing to the celebrity.
    Because we are using randomly selected values, your results will often look different, but similar.
    """

    from random import randrange

    n = len(crowd)  # first determine the size of the crowd.
    celebrity = randrange(n)  # a random number is chosen to select the celeb. The celeb knows no one else.

    for i in range(n):
        crowd[i][celebrity] = True  # In this position, this person (i) has a connection to the celebrity (True)
        crowd[celebrity][i] = False  # This is the celebrity. It has no edges to any of the others (False)
        # All other values are either zero or one, indicating that someone may know someone
        # else in the crowd.

    return crowd


def find_celebrity_bruteforce(crowd: list) -> int:
    """
    A simple attempt to work through all the members of the crowd to find the celebrity.
    We could inspect each vertcie. Then each edge. Break if we found an edge, try the next until we're sure this
    vertice has no edges.

    :param crowd: A graph, in which one vertice has no outbound edges.
    :return: The vertice who is the celebrity.

    >>> celeb = find_celebrity_bruteforce(make_celebrity_in_crowd(get_random_graph(50))) # doctest: +ELLIPSIS
    >>> print ("Found a celebrity at pos :%s:" % celeb) # doctest: +ELLIPSIS
    Found a celebrity ...
    """

    n = len(crowd)  # find out how many vertices we have.

    for v in range(n):  # walk through the vertices
        for e in range(n):  # walk through the edges
            if crowd[v][e] is not False:  # Candidate knows someone, not a celeb
                break
            if crowd[e][v] is not True:
                break  # the other person doesn't know this vertice
            if e == v:
                continue  # don't care if you have an edge to yourself.
            else:
                return v  # winner winner chicken dinner!
    return None  # Boo! Didn't find a celebrity


def find_celebrity(crowd: list) -> int:
    """
    A simple attempt to work through all the members of the crowd to find the celebrity.
    We could inspect each vertcie. Then each edge. Break if we found an edge, try the next until we're sure this
    vertice has no edges.

    :param crowd: A graph, in which one vertice has no outbound edges.
    :return: The vertice who is the celebrity.

    >>> celeb = find_celebrity(make_celebrity_in_crowd(get_random_graph(31)))
    >>> print ("Found a celebrity at pos :%s:" % celeb)
    Found a celebrity ...:
    """

    n = len(crowd)  # find out how many vertices we have.
    primary, secondary = 0, 1

    # Do some optimising primary

    for candidate in range(2, n + 1):  # We will only be looking from candidate 3 on
        if crowd[primary][secondary]:  # does person one know this candidate?
            primary = candidate  # If so, person one isn't our celeb. So replace p1 with the candidate celeb
        else:
            secondary = candidate  # Person one didn't know our candidate. Still maybe a celeb.
            # So promote the candidate to secondary position

    if primary == n:  # If our new primary position is the last person in the crowd
        candidate = secondary  # we'll promote our secondary cadidate to candidate
    else:
        candidate = primary  # our prime wasn't the last in the crowd, so is now our candidate

    # Now that we have a candidate, we need to make sure

    for v in range(n):  # walk through the vertices
        if candidate == v:  # this is ourself
            continue
        if crowd[candidate][v] is True:  # candodate knows a rando, not a celeb
            break
        if crowd[v][candidate] is not True:  # rando doesn't know this candidate, so not a celeb
            break
        else:
            return candidate  # winner winner chicken dinner!
        return None  # Boo! Didn't find a celebrity
