"""
Reversing the order of word in a string is not the same as reversing the order of a string.
For example, reversing the words in the string:
    'chocolate like I'
should give:
    'I like chocolate'
and not:
    'I ekil etalocohc'
"""


def reverse_word_order_loops(sentence: str) -> str:
    """
    This is one way to reverse a string using loops

    >>> reverse_word_order_loops('chocolate like I')
    'I like chocolate'

    """

    retvalue: str = ''
    for word in sentence.split():
        retvalue = word + ' ' + retvalue

    return retvalue.rstrip()


def reverse_word_order_join(sentence: str) -> str:
    """
    We can use split, array slice, and join to do this concisely

    >>> reverse_word_order_join('chocolate like I')
    'I like chocolate'

    """
    # split words of string separated by space
    words: list = sentence.split()

    # reverse list of words
    words = words[-1::-1]

    # now join words with space
    return ' '.join(words)


def reverse_word_order_reversed(sentence: str) -> str:
    """
    This is the most concise and most pythonic version, IMO.

    >>> reverse_word_order_reversed('chocolate like I')
    'I like chocolate'

    """
    return ' '.join(reversed(sentence.split()))


def reverse_word_order_no_library(sentence: str) -> str:
    """
    The question may sometimes say "without using a library method".
    The real answer is WTF? Library methods - especially in Python - are highly optimized C code and likely to be
    much faster and use less memory than anything you code yourself. Don't reinvent the wheel!
    Nevertheless, here we go.

    >>> reverse_word_order_no_library('chocolate like I')
    'I like chocolate'

    """
    rpos = len(sentence) - 1

    current_word: str = ''
    output: str = ''

    # walk the string backwards
    while rpos > -1:
        if sentence[rpos] == ' ':  # until we find a space (word boundary)
            output = output + ' ' + current_word
            current_word = ''
        else:
            # build the current word
            current_word = sentence[rpos] + current_word

        rpos = rpos - 1

    output = output + ' ' + current_word

    return output.lstrip()
