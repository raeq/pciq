"""
As orginally posed for Java, the coding interview question "How do you find the duplicate number on a given integer
array?" needs to be reformulated for using the more pythonic list data type. In some ways this is the inverse
of the missing integer question. The same techniques can be used - but just inverted so that we're not looking for
the missing elements in a sequence, but for additional elements.
"""


def duplicate_number_naieve(numbers: list) -> list:
    """
    This is a simple solution, and works well only if the list of integers is small
    The issue is the count() method in the final statement - this is time consuming, and is repeated for each element.

    >>> duplicate_number_naieve([1, 2, 2, 3, 5, 5, 4, 5, 50, 4, 5] + list(range(100, 1000)))
    [2, 4, 5]

    :param numbers: A list of integers which may contain duplicates
    :return: A list of integers occurring twice or more in numbers
    """

    # The ideal set only contains unique items from the list of integers
    # The reason for doing this is to reduce the number of iterations in the final statement.
    # If our list of integers contain 1M repetitions of the number 5, this program would iterate that many
    # times in the comprehension below.
    ideal_set: set = set(sorted(numbers))   # An ideal perfect sequence with each integer occurring only once

    # return every element in ideal_set if it occurs more than
    # once in the supplied list of integers
    return [x for x in ideal_set if numbers.count(x) > 1]


def duplicate_number_fast(numbers: list) -> list:
    """
    This is a more convoluted solution, it works well  on large lists.

    >>> duplicate_number_fast([1, 2, 2, 3, 5, 5, 4, 5, 50000, 4, 5] + list(range(1000, 10000)))
    [2, 4, 5]

    :param numbers: A list of integers which may contain duplicates
    :return: A list of integers occurring twice or more in numbers
    """

    seen: set = set() # Sets are unique
    seen2: set = set()
    seen_add = seen.add # This direct reference to the add function avoids a dictionary lookup on the object
    seen2_add = seen2.add

    for item in numbers:
        if item in seen:
            seen2_add(item) # Found a duplicate
        else:
            seen_add(item) # Not yet encountered this item
    return list(sorted(seen2))
