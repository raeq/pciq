"""
Here we'll deal with coroutines, in their different forms.
First we'll try a hello world coroutine, as simple as can be.

One thing to note: before a coroutine can receive a message, it needs to be primed with the __next__ method.

Coroutines are also an interesting way to build processing pipelines, each coroutine handling
a separate job, or "concern".
"""

from functools import wraps


def coroutine(func):
    """
    This is a decorator. When applied to a function as @coroutine,it does nothing except automatically
    perform an initiating __next__() call, because this is practically always needed.
    """

    @wraps(func)
    def start(*args, **kwargs):
        cr = func(*args, **kwargs)
        cr.__next__()
        return cr

    return start


def producer(sentence, next_coroutine):
    """
    Here we create a pipeline.
    The pipeline has 3 stages:
        One: invert a token (token_invert)
        Two; Test to see if the token contains a string (token_filter)
        Three: print the string  (print_token)
    We define these in reverse order, because each takes a coroutine as an argument.

    >>> pt = token_print()
    >>> pf = token_filter(pattern='in', next_coroutine=pt)    # the pattern filter registers the print token coroutine
    >>> pi = token_invert(next_coroutine=pf)
    >>> sentence = "Bob is running behind a fast moving car"
    >>> producer(sentence, pi)                                  # Here, the pattern filter is the coroutine to be called
    gninnur
    """
    tokens = sentence.split()
    for token in tokens:
        if next_coroutine is not None:
            next_coroutine.send(token)
    next_coroutine.close()


@coroutine
def token_invert(next_coroutine=None):
    """
    This is one coroutine which can be used in a pipeline.
    It simply reverses any text it is sent.
    """
    try:
        while True:
            token: str = (yield)
            if not isinstance(token, str):
                raise ValueError("This pipeline works on strings only.")

            token = token[::-1]  # use a reverse slice, starting at the last letter
            if next_coroutine is not None:
                next_coroutine.send(token)
    except GeneratorExit:
        pass


@coroutine
def token_filter(pattern="ing", next_coroutine=None):
    '''
    This coroutine was initialized with a pattern, and each iteration yields a token which was sent.
    If the pattern is contained in the token, the token is sent to the next coroutine present, if any.
    '''
    try:
        while True:
            token: str = (yield)
            if not isinstance(token, str):
                raise ValueError("This pipeline works on strings only.")

            if pattern in token:
                if next_coroutine is not None:
                    next_coroutine.send(token)
    except GeneratorExit:
        pass


@coroutine
def token_print(next_coroutine=None):
    """
    This coroutine will print any tokens sent to it.
    If there are any additional co_routines, they will be invoked to.
    """
    try:
        while True:
            token: str = (yield)
            if not isinstance(token, str):
                raise ValueError("This pipeline works on strings only.")

            print(token)
            if next_coroutine is not None:
                next_coroutine.send(token)

    except GeneratorExit:
        pass


@coroutine
def helloworld(prefix: str):
    """

    >>> coroutine = helloworld("Hello")
    >>> coroutine.send("World!"); next(coroutine)
    'Hello World!'
    >>> coroutine.send("Children!"); next(coroutine)
    'Hello Children!'
    >>> coroutine.send("Everyone!"); next(coroutine)
    'Hello Everyone!'
    >>> coroutine.close()
    """

    try:
        while True:
            retval = 'aa'
            name = yield
            if len(name) > 0:
                retval = ("%s %s" % (prefix, name))
            yield retval
    except GeneratorExit:
        pass
