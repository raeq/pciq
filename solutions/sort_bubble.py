"""
How do you implement the bubble sort algorithm in Python?
Well, you wouldn't, it's a terrible way to sort! Look at the insertion sort in preference :)
"""


def bubble_sort_simple(l: list) -> list:
    """
    This works by having two iterators.
    On the first run, the inner loop moves the largest item it finds to the end of the list.
    On the second loop, the second largest item is moved to the end of the list -1
    And so on.
    >>> bubble_sort_simple([10, 5, 100, 300, 1, 60])
    [1, 5, 10, 60, 100, 300]
    """
    number_passes = len(l) - 1

    for decrementing_iterator in range(number_passes, 0, -1):  # outer iterator decrementing

        for index in range(decrementing_iterator):  # inner iterator incrementing

            if l[index] > l[index + 1]:
                l[index], l[index + 1] = l[index + 1], l[index]  # swap them in place

    return l


def bubble_sort_optimized(l: list) -> list:
    """
    This works in the same way as the previous algorithm - with one shortcut optimization.
    Instead of doing n-1 passes through the list, we'll quit if we detect that the current iteration
    already produced a sorted list.
    >>> bubble_sort_optimized([1, 5, 10, 8, 60, 100, 300])
    [1, 5, 8, 10, 60, 100, 300]
    """
    made_exchange: bool = True  # we'll record whether or not an swap exchange was made. We'll assume we need a pass
    number_passes = len(l) - 1

    for decrementing_iterator in range(number_passes, 0, -1):  # outer iterator decrementing
        made_exchange = False  # well, we haven'tmade any exchanges on this pass yet!

        for index in range(decrementing_iterator):  # inner iterator incrementing
            if l[index] > l[index + 1]:
                made_exchange = True  # OK, we made an exchange on this pass
                l[index], l[index + 1] = l[index + 1], l[index]  # swap them in place

        if not made_exchange:
            # This is the optimization. We made an entire pass without swapping elements, so the list is sorted.
            break

    return l
