"""
How can a string be reversed using recursion?
Huh, why though? This is a horribly complex way to do this. But whatever, they asked so here goes.
"""


def reverse_recursion(word: str) -> str:
    """
    Recursion is really a stupid way to this in Python.
    >>> reverse_recursion('dear')
    'raed'

    """

    def recurse(word1: str) -> str:
        """
        On the first run, we return 'ear' + 'd'
        On the second run, we return 'ar' + 'e' + 'd'
        On the third run, we return 'r' + 'a' + 'e' + 'd'
        On the fourth run, we return '' + 'r' + 'a' + 'e' + 'd'
        """
        if word1 == "":
            return word1
        return recurse(word1[1:]) + word1[0]

    return recurse(word)


def reverse_sane(word: str) -> str:
    """
    This is the sane version.
    Just use a slice, start at the end, then working in steps of -1 to go to the beginning of the word...
    >>> reverse_sane('dear')
    'raed'
    """
    return word[::-1]


def reverse_inbuilt(word: str) -> str:
    """
    This is a pretty laze way to do this, using the reversed() method.
    Note that reversed() returns an iterator. Force it to return all values by converting it to a list()
    and then join up each of the list's elements.
    >>> reverse_inbuilt('dear')
    'raed'
    """
    return ''.join(list(reversed(word)))
