"""
The insertion sort.
"Not quite as bad as the bubble sort!"
"""


def sort_insertion(l: list) -> list:
    """

    :param l: An unsorted list.
    :return: A sorted list.
    >>> sort_insertion([10, 5, 100, 300, 1, 60])
    [1, 5, 10, 60, 100, 300]
    """

    n: int = len(l)
    i: int = 1

    # iterate over the list from left to right
    while i <= n - 1:

        # get the current value in the list, the one we want to sort into the right place at the beginning
        # of the list
        value = l[i]
        position = i

        # if we're at the first position, do nothing
        # if tis position is already larger than it's predecssorm it's already sorted
        while position > 0 and l[position - 1] > value:
            l[position] = l[position - 1]
            position = position - 1

        i = i + 1
        l[position] = value

    return l
