"""
Exception handling is how a Python program reacts to error conditions.
One theory says that all assertions created during the development phase should eventually be replaced
with exceptions. YMMV.
"""


def exceptions1(value: str):
    """
    >>> exceptions1('Hi!')
    'Hi!'
    >>> exceptions1('hi!')
    Traceback (most recent call last):
    ...
    RuntimeError: Oops!
    >>> exceptions1(1)
    Traceback (most recent call last):
    ...
    TypeError: Only send strings, please.
    >>> exceptions1('Really Long String Which Is Not OK!!')
    Traceback (most recent call last):
    ...
    ValueError: I only like short strings.
    """
    if not isinstance(value, str):
        raise TypeError("Only send strings, please.")
    elif len(value) >= 10:
        raise ValueError("I only like short strings.")
    elif value.title() != value:
        raise RuntimeError("Oops!")
    else:
        return value


class MyError(Exception):
    """
    This is the base exception class. Specific exceptions for our program are derived from here.
    This primarily has the job of logging all exceptions.
    """

    def __init__(self, message):
        import logging
        logger = logging.getLogger()  # Only if defined elsewhere
        super().__init__(message)
        self.message = message
        logger.exception(message)


class StringTooLongException(MyError):
    """Our user-defined error describing strings which are too long."""
    __slots__ = ['msg']

    def __init__(self, msg, error_string: str):
        super(StringTooLongException, self).__init__(msg)
        self.msg = "Supplied string \'{}\' is not of the correct length: \'{}\'".format(error_string,
                                                                                        len(error_string))

    def __str__(self):
        return self.msg


class StringNotTitleCaseException(MyError):
    """A user-defined exception indicating that a string Is Not Title Case."""
    __slots__ = ['msg']

    def __init__(self, msg, error_string: str):
        super(StringNotTitleCaseException, self).__init__(msg)
        self.msg = "Supplied string \'{}\' is not of the correct title case: \'{}\'".format(error_string,
                                                                                            error_string.title())

    def __str__(self):
        return self.msg


def exceptions2(value: str):
    """
    >>> exceptions2('Hi!')
    'Hi!'
    >>> exceptions2('lower case')
    Traceback (most recent call last):
    ...
    solutions.exceptions.StringNotTitleCaseException: Supplied string 'lower case' is not of the correct title case: 'Lower Case'
    >>> exceptions2(1)
    Traceback (most recent call last):
    ...
    TypeError: Only send strings, please.
    >>> exceptions2('Really Long String Which Is Not OK!!')
    Traceback (most recent call last):
    ...
    solutions.exceptions.StringTooLongException: Supplied string 'Really Long String Which Is Not OK!!' is not of the correct length: '36'
    """
    if not isinstance(value, str):
        raise TypeError("Only send strings, please.")
    elif len(value) > 10:
        raise StringTooLongException("I only like short strings.", value)
    elif value.title() != value:
        raise StringNotTitleCaseException("Oops!", value)
    else:
        return value
