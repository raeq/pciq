"""
In python, "callables" are a class of language structure which, well, can be called.
Typically these are functions, closures and coroutines. But lambdas can also be called.

Lambda expressions (sometimes called lambda forms) are used to create anonymous functions.
The expression lambda parameters: expression yields a function object.
Note that functions created with lambda expressions cannot contain statements or annotations.
https://docs.python.org/3/reference/expressions.html#lambda

"""


def hello_lambda():
    """
    Just for the sake of packaging, the following lambda is inside a function. It doesn't need to be -
    it can be anywhere.

    However please note that these trivial examples are not how we use lambdas. PEP-8 specifically prohibits it!
    Instead of having these weird anonymous functions breaking the stack trace, it's better to define a named
    def. You can also put multiple statements in a def.

    This is strictly for illustration only :)

    >>> print(hello_lambda())
    !lo le
    """
    # lambda's are fairly straightforward.
    # define a signature with NAME = lambda
    # specify the parameters with PARAMS:
    # then write the expression to apply to the function to the right of the colon
    # you can chain them together like function calls

    hello = lambda val: "Hello " + val  # simple lambda to create Hello World!
    reverse = lambda val, step: val[::step]  # simple lambda with two parameters to reverse a string
    return reverse(hello("World!"), -2)  # Add "World!" to "Hello ", reverse it, return every second letter, RTL


def sorting_lambda(sortme: list):
    """
    We usually use lambdas to do simple, repetitive operations or calculations. Or sorting.
    In this example, we have a list containing 4 tuples - each tuple contains a country name and its population.
    We want to sort this list based on the population...

    >>> c1 = ('France', 69000000)
    >>> c2 = ('India', 900000000)
    >>> c3 = ('USA', 350000000)
    >>> c4 = ('Vatican', 10000)
    >>> l = [c1, c2, c3, c4]
    >>> print (l)
    [('France', 69000000), ('India', 900000000), ('USA', 350000000), ('Vatican', 10000)]
    >>> print (sorting_lambda(l))
    [('Vatican', 10000), ('France', 69000000), ('USA', 350000000), ('India', 900000000)]
    """
    sortme.sort(key=lambda x: x[1])  # This lambda is passed a list item and returns element 1 as the sort key
    return sortme
