"""
Questions of the form "each member visits every other member once" indicates the presence of a complete graph.
https://en.wikipedia.org/wiki/Complete_graph Every possible pair of vertices (nodes) has a unique edge (connection).

Once the problem has been identified, the following equation can be applied.



One equation n(n − 1)/2 (where n = the number of vertices/nodes).
It works by first calculating double the correct number (because a <-> b is counted as well as b <-> a) and
then halving that result.

A different method (Gaussian) way is to know that, for example:
    1 + 100 = 101
    2 + 99 = 101
    3 + 98 = 101
    4 + 97 = 101
    etc.
So simply sum all the integers between one and n-1 = range(n).



"""


def count_edges_in_complete_graph_classic(vertice_count: int):
    """
    Get the edge count in a 128-clique.

    >>> count_edges_in_complete_graph_classic(128)
    8128
    """
    return int(vertice_count * (vertice_count - 1) / 2)


def count_edges_in_complete_graph_gauss(vertice_count: int):
    """
    Get the edge count in a 128-clique.

    >>> count_edges_in_complete_graph_gauss(128)
    8128
    """
    return sum(i for i in range(vertice_count))  # use list comprehension to generate 1 to n-1.
