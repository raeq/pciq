"""
How can you tell if two rectangles overlap?

Well, first we need a rectangle object in coordinate space!

Here is how you instantiate a new rectangle:
Remember, x is horizontal, y is vertical (y goes to the sky!)

>>> r: Rectangle = Rectangle(10, 110, 10, 60)
>>> r.width
50
>>> r.length
100
>>> r.perimeter
300
>>> r.area
5000
>>> r.is_square
False
>>> print(r)
R[10,110,10,60], s=False, l=100, w=50, a=5000, p=300, d=111.80339887498948
"""


class Rectangle(object):
    """
    A rectangle in coordinate space.
    Instantiated by specifying its position.
    """
    __slots__ = ['min_x', 'max_x', 'min_y', 'max_y']

    def __init__(self, min_x=0, max_x=0, min_y=0, max_y=0):

        if min_x > max_x:
            raise ValueError
        else:
            self.min_x = min_x
            self.max_x = max_x

        if min_y > max_y:
            raise ValueError
        else:
            self.min_y = min_y
            self.max_y = max_y

    def is_intersect(self, other):
        """
        Does this rectangle overlap with a different rectangle in this coordinate space?

        >>> r: Rectangle = Rectangle(10, 110, 10, 60)
        >>> l: Rectangle = Rectangle(100, 200, 20, 70)
        >>> r.is_intersect(l)
        True
        >>> r: Rectangle = Rectangle(10, 110, 10, 60)
        >>> l: Rectangle = Rectangle(120, 230, 70, 120)
        >>> r.is_intersect(l)
        False
        """
        if self.min_x > other.max_x or self.max_x < other.min_x:
            return False
        if self.min_y > other.max_y or self.max_y < other.min_y:
            return False
        return True

    def __and__(self, other):
        """
        Return a rectangle of minimum coordinates where two rectangles overlap.
        """
        if not self.is_intersect(other):
            return Rectangle()  # return a new rectangle object with 0 dimensions
        min_x = max(self.min_x, other.min_x)
        max_x = min(self.max_x, other.max_x)
        min_y = max(self.min_y, other.min_y)
        max_y = min(self.max_y, other.max_y)
        return Rectangle(min_x, max_x, min_y, max_y)

    intersect = __and__

    def __or__(self, other):
        """
        Return the maximum coordinates if both rectangles were expanded to encompass each other.
        """
        min_x = min(self.min_x, other.min_x)
        max_x = max(self.max_x, other.max_x)
        min_y = min(self.min_y, other.min_y)
        max_y = max(self.max_y, other.max_y)
        return Rectangle(min_x, max_x, min_y, max_y)

    union = __or__

    def __eq__(self, other):
        """
        >>> r: Rectangle = Rectangle(10, 110, 10, 60)
        >>> l: Rectangle = Rectangle(10, 110, 10, 60)
        >>> r == l
        True
        """
        if isinstance(other, self.__class__):
            return self.__repr__() == other.__repr__()
        return False

    def __ne__(self, other):
        """
        >>> r: Rectangle = Rectangle(10, 110, 10, 60)
        >>> l: Rectangle = Rectangle(10, 110, 10, 60)
        >>> r != l
        False
        >>> r != 'something not a rectangle'
        True
        """
        if isinstance(other, self.__class__):
            return self.__repr__() != other.__repr__()
        return True

    def __repr__(self):
        return f'R[{self.min_x},{self.max_x},{self.min_y},{self.max_y}], s={self.is_square}, l={self.length}, w={self.width}, a={self.area}, p={self.perimeter}, d={self.diagonal}'

    @property
    def width(self):
        """
        Returns the width (height) of this rectangle.
        """
        return self.max_y - self.min_y

    @property
    def length(self):
        """
        Returns the length of this rectangle.
        """
        return self.max_x - self.min_x

    @property
    def area(self):
        """
        Returns the area of this rectangle
        """
        return self.length * self.width

    @property
    def perimeter(self):
        """
        Returns the perimeter of this rectangle.
        """
        return (self.length + self.width) * 2

    @property
    def diagonal(self):
        """
        Returns the length of this rectangle's diagonal.
        """
        return pow(pow(self.width, 2) + (pow(self.length, 2)), 0.5)

    @property
    def is_square(self) -> bool:
        """
        Returns true if this rectangle is a square.
        """
        return self.length is self.width


if __name__ == "__main__":
    import doctest

    doctest.testmod()
