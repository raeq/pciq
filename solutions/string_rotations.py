"""
Figuring out if one string is a rotated version of another is a very common interview question.
But what the hell are we talking about when we talk about a "rotation"?

Imagine you have a word like 'fantastical" and you print it out on a ribbon. Then you tape the ends of
the ribbon together to make a ring. You can then start at any point:

    'fantastical'
    'antasticalf'
    'ntasticalfa'
    'asticalfant'

And see that all of these are rotations of each other. So how would we show that, for example,
'asticalfant' is a rotation of 'fantastical' ?

"""


def is_rotation(word1: str, word2: str) -> bool:
    """
    This is a pretty elegant solution. The "aha" moment is realising that if you double the word to:
        'fantasticalfantastical'
    Then at some point along this doubled word, every valid rotation can be found.

    >>> is_rotation('fantastical', 'asticalfant')
    True
    >>> is_rotation('fantastical', 'saticalfant')
    False
    >>> is_rotation('fantastical', 'fantastical')
    False
    >>> is_rotation('fantastical', 'something else entirely')
    False
    """

    # obviously not a rotation if the two words or of a different length
    if len(word1) != len(word2):
        return False

    # obviously also not a rotation if the two words or the same
    if word1 is word2:
        return False

    return word2 in word1 * 2


def is_rotation_slice(word1: str, word2: str) -> bool:
    """
    OK so the first solution was nice and easy - but it requires a lot of memory if we're talking huge strings.
    Maybe more memory than you have. So let's try a solution which uses loops.

    This works like this:
        Start with 'f'
            does word 2 start with 'f'? No, keep going until we find where it starts

        So we find 'ant' in word2 at position 4.
            Does word 2 start with 'astitcal'
                Yes
            Does word2 and with 'fant' - the first 4 letters of word 1?
                Yes.
                We have a winner.


    >>> is_rotation_slice('fantastical', 'asticalfant')
    True
    >>> is_rotation_slice('fantastical', 'saticalfant')
    False
    >>> is_rotation_slice('fantastical', 'fantastical')
    False
    >>> is_rotation_slice('fantastical', 'fantastically')
    False

    """

    # Again, do some sanity checking as previously.
    if len(word1) != len(word2):
        return False

    if word1 is word2:
        return False

    for index in range(len(word1)):
        if word2.startswith(word1[index:]) and word2.endswith(word1[:index]):
            return True

    return False
