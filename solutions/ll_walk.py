"""
How to walk a linked list?
This code uses the Node class found in the module .linkedlist.py
"""

from . import linkedlist as ll


def walk_linked_list():
    """
    >>> walk_linked_list()
    first
    second
    third
    """
    n: ll.Node = ll.Node("first")
    n.next = ll.Node("second")
    n.next.next = ll.Node("third")

    while n:
        print(n)
        n = n.next
