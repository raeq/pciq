"""
A Node class which implements linked lists
"""


class Node:
    """
    This is node class, and can be used to create linked lists!
    It implements both a next and a previous method, allowing one to traverse forwards and backwards.
    It also uses the __slots__ capability for memory efficient class properties.

    >>> n: Node = Node('second')
    >>> n.previous = Node('first')
    >>> n.next = Node('third')
    >>> print(n.previous, n, n.next)
    first second third
    """
    __slots__ = ['_payload', '_next', '_previous']

    def __init__(self, payload: object = None):
        self._next = None
        self._previous = None
        self._payload = payload

    @property
    def payload(self):
        return self._payload

    @payload.setter
    def payload(self, value):
        self._payload = value

    @property
    def next(self):
        return self._next

    @next.setter
    def next(self, value):
        if value is not None:
            self._next = value
            self._next._previous = self

    @property
    def previous(self):
        return self._previous

    @previous.setter
    def previous(self, value):
        if value is not None:
            self._previous = value
            self._previous._next = self

    def __str__(self) -> str:
        return str(self.payload)

    def __repr__(self) -> str:
        return self.__str__()
