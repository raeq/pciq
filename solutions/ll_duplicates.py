"""
How do you find and remove duplicates in a linked list?

"""

from . import linkedlist as ll


def linked_list_remove_duplicates(head: ll.Node) -> ll.Node:
    """
    This method receives a linked list, removes duplicates, and returns the head

    >>> n1: ll.Node = ll.Node('first')
    >>> n2: ll.Node = ll.Node('second')
    >>> n3: ll.Node = ll.Node('second')
    >>> n4: ll.Node = ll.Node('second')
    >>> n5: ll.Node = ll.Node('third')
    >>> n6: ll.Node = ll.Node('second')

    >>> n1.next = n2
    >>> n2.next = n3
    >>> n3.next = n4
    >>> n4.next = n5
    >>> n5.next = n6


    third
    """

    current = nextone = head  # make two copies of the original head node
    while current is not None:  # as long as the current node is not None
        while nextone.next is not None:  # as long as the next node is not None
            if nextone.next.payload == current.payload:  # check nextone.next.payload to see if it is the same
                nextone.next = nextone.next.next  # skip nextone.next out of the list
            else:
                nextone = nextone.next  # put this line in an else, to avoid skipping items
        current = nextone = current.next

    return head
