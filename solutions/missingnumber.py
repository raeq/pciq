"""
How do you find the missing number in a given integer sequence?

The classical approach is simple. You can calculate the sum of values n (n'x...n'y). This is the aggregate
value when no element is missing. Then sum all of your given values. The difference between the two sums is
the value of the missing element.

However, the naieve approach quickly becomes useless when there are more than one missing values. Other simple approaches
fail when there are several consecutive missing numbers in the list.

Here we will see a bunch of solutions - and the naieve method - for finding the missing elements in a list of numbers
which should be consecutively sequentially incrementing by one.

The ideal list with no missing elements might be [3, 4, 5, 6], and the comparator with missing elements [3, 6].

"""


def missing_number_comprehension_bruteforce(number_list: set) -> list:
    """

    :param number_list: A list of numbers with maybe values missing.
    :return: The values missing in the provided number list.

    first create a new but complete list.
    then go through every element in the complete list, and return it if it isn't in the test list. This isn't at all
    efficient, but it works well for human-scale list sizes.

    Using List comprehension in this way is minimalistic, but can be difficult to read without some experience.

    >>> numbers = [6, 5, 11, 1, 3, 10, 8, 2, 7, 9, 9, 15]

    Test to see if the bruteforce method returns the correct result.

    >>> missing_number_comprehension_bruteforce(numbers)
    [4, 12, 13, 14]

    """
    number_list = sorted(number_list)
    complete_number_list: list = list(range(min(number_list), max(number_list) + 1))
    return [x for x in complete_number_list if x not in number_list]


def missing_number_recursion_bruteforce(number_list: list, start: int, end: int) -> list:
    """

    :param start:
    :param end:

    :param number_list: A list of numbers with maybe values missing.
    :return: The values missing in the provided number list.

    Brute forcing the search for a missing number works, but it has an unpredictable duration.
    The larger the list of numbers, the longer it will take. Also, it is possible to measure
    execution time differences depending on if the missing number is near the beginning, or near the end, of a long
    list.
    This method uses recursion to continually halve the search space.
    This method fails if the supplied number_list had not been sorted.

    >>> numbers = [6, 5, 11, 1, 3, 10, 8, 2, 7, 9, 9, 15]
    >>> list(missing_number_recursion_bruteforce(sorted(numbers), 0, len(numbers)-1))
    [4, 12, 13, 14]

    """

    # Don't split the list further if we've already gone down to consecutive elements
    if end - start <= 1:
        # Test to see if the first element of this pair is more than 1 integer away from the last
        if number_list[end] - number_list[start] > 1:
            # Found at least one missing element
            # Return (keep yielding) all of the missing integers between the first and last in the pair
            yield from range(number_list[start] + 1, number_list[end])
        return

    # Find the midpoint in the list, this is our index
    index: int = start + (end - start) // 2

    # If the first halve of the list is not consecutive
    if number_list[index] != number_list[start] + (index - start):
        # Recurse through to find the 1/4, or 1/8 of the list which has a gap
        yield from missing_number_recursion_bruteforce(number_list, start, index)

    # If the second halve of the list is not consecutive
    if number_list[index] != number_list[end] - (index - end):
        # Recurse through to find the 1/4, or 1/8 of the list which has a gap
        yield from missing_number_recursion_bruteforce(number_list, index, end)


def missing_numbers_set_difference(number_list: list) -> list:
    """

    :param number_list: A list of numbers with maybe values missing.
    :return: The values missing in the provided number list.

    We can think about this problem in terms of set theory. You've seen a Venn diagram, with two circles overlap.
    A blue and a red circle will be green where they intersect - overlap. If the green circle contains our
    given numbers, and the red circles contain all numbers in the range, our question can be posed as "what is red
    minus green: what is the difference from red to green?".

    >>> numbers = [6, 5, 11, 1, 3, 10, 8, 2, 7, 9, 9, 15]
    >>> missing_numbers_set_difference(numbers)
    [4, 12, 13, 14]
    """
    # Get the lowest and highest values in our provided list of numbers
    start, end = min(number_list), max(number_list)

    # use the range() function to create a set of complete numbers
    # and use the difference() method to find the difference from our provided numbers
    return sorted(set(range(start, end + 1)).difference(number_list))


def missing_numbers_naieve(number_list: list) -> int:
    """
    :param number_list: A list of numbers with maybe values missing.
    :return: The values missing in the provided number list.

    This method only works if there is a single missing number, it can't detect several.
    The main premise is that if you have a contiguous sequence, its sum can be compared to a sequence with an element
    missing.
    The difference is the value of the missing element.


    >>> numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15]

    Test to see that the naieve method works.

    >>> missing_numbers_naieve(sorted(numbers))
    14
    """

    number_list = sorted(number_list)
    start, end = min(number_list), max(number_list)
    return sum(list(range(start, end + 1))) - sum(number_list)


def missing_numbers_iterative(number_list: list) -> list:
    """

    :param number_list: A list of numbers with maybe values missing.
    :return: The values missing in the provided number list.

    This time we will sort the provided list so that it is sequential.
    Then we will use the sliding windows recipe provided by the python documentation team.

    This is an interesting solution, which gives up its secrets only after some study. The magic sauce here are the
    two methods chain and islice from the itertools module, and the tuple method.

    In essence, the chain method will call the window function every time its condition is met, namely when a missing
    number is found.


    >>> numbers = [1, 2, 3, 4, 6, 7, 10, 11, 12, 13, 15]
    >>> missing_numbers_iterative(numbers)
    [5, 8, 9, 14]

    """
    from itertools import islice, chain

    number_list = sorted(number_list)

    def window(number_list: list, node: int = 2) -> list:
        my_iterator = iter(number_list)

        result = tuple(islice(my_iterator, node))
        if len(result) == node:
            yield result
        for elem in my_iterator:
            result = result[1:] + (elem,)
            yield result

    missing = chain.from_iterable(
        range(x + 1, y) for x, y in window(number_list) if (y - x) > 1
    )

    return list(missing)


def missing_number_simple(numbers: list, smallest: int = None, largest: int = None) -> list:
    """
    This is probably the next simplest way to solve this problem in python after missing_number_comprehension_bruteforce.
    Again we use the range() method to build a complete list of all possible integers between the user supplied
    smallest and largest values. (If the user does not supply values for smallest and largest, the min and max values
    in the user supplied list are used).

    This function uses python's neat trick of list comprehension to return a list of x built from all of the x values
    in the range not supplied and already "seen".

    :param numbers: The list of integers to be checked for missing values.
    :param smallest: If present, the smallest of the values at the lower bound
    :param largest: If present, the highest of the values of the upper bound.
    :return: A list of integers missing in the sequence.

    >>> numbers = [1, 2, 3, 4, 6, 7, 10, 11, 12, 13, 15]
    >>> missing_number_simple(numbers, 4, 15)
    [5, 8, 9, 14]
    >>> missing_number_simple(numbers)
    [5, 8, 9, 14]

    """
    if smallest is None:
        smallest = min(numbers)

    if largest is None:
        largest = max(numbers)

    seen = set(sorted(numbers))
    return [x for x in range(smallest, largest + 1) if x not in seen]

