"""
This is code challenge 58 from PyBites at https://pybit.es/codechallenge58.html

Week 1

    Pick your favorite podcast, make sure it is one with quite some data (episodes) and transcripts.
    Make a script to get all the transcripts. As this could involve retrieving data from Github (Talk Python),
    feed parsing / web crawling, even extracting data from PDFs (Tim Ferriss Show - episodes 1-150), we
    decided to split this into two challenges.
    Store the results somewhere, for example an (sqlite) database.

Week 2

    Make a virtual environment and pip install NLTK / Natural Language Toolkit.
    Read up on how to use the library.
    From here on we leave you totally free to find the patterns in the data that you are interested in:
    sentiments, book recommendations, you name it.
    Show the results in a notebook or in any way you like.

"""

from urllib.parse import urlparse, urljoin
from urllib.request import urlopen

import apsw
import nltk
from bs4 import BeautifulSoup


def transcript_factory(cursor, row) -> dict:
    """
    This is a row factory. Tuples are usually returned from an sqlite query, but I want a dict sturcture
    https://docs.python.org/3.6/library/sqlite3.html#sqlite3.Connection.row_factory

    :param cursor: A valid cursor
    :param row: A row
    :return: A dictionary
    """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def get_transcripts(url: str):
    """
    This function is specific to the "Talk Python" podcast.
    It fetches the transcripts of podcasts from GitHub and stores them in a local sqlite3 database
    If run again, it won't refetch transcripts already in the database


    Don't run these tests automatically, they take a long time.
    # >>> get_transcripts('https://github.com/mikeckennedy/talk-python-transcripts/tree/master/transcripts')
    """

    # Create the database and create the transcripts table
    connection = apsw.Connection("mikeckennedy.sqlite")
    connection.setrowtrace(transcript_factory)
    cursor = connection.cursor()

    cursor.execute(
        'create table IF NOT EXISTS transcripts(pk TEXT, transcript TEXT constraint transcripts_pk primary key);')

    html: str = ''

    rows = list(cursor.execute("select transcript, pk from transcripts where pk = ?  LIMIT 1", ('INDEX',)))
    if len(rows) == 1:
        html = rows[0]['transcript']
    else:
        u = urlparse(url)
        html = urlopen(url).read().decode()

        cursor.execute('INSERT OR IGNORE INTO transcripts (pk, transcript) VALUES (?, ?)',
                       ('INDEX', html,))

    soup = BeautifulSoup(html, "html5lib")

    # Get all links on the page which might link to a transcript
    for link in soup.find_all('a', 'js-navigation-open'):
        if link.get('href').endswith('.txt'):
            title = link.get('title')  # Get the TITLE of the link. This is our PK in sqlite

            # See if we already have this transcript
            rows = list(cursor.execute("select pk, transcript from transcripts where pk = ?  LIMIT 1", (title,)))
            if len(rows) < 1:

                # If not, get the github page of the transcript
                detail_page = urlopen(urljoin(url, link.get('href'))).read().decode()
                soup2 = BeautifulSoup(detail_page, "html5lib")

                # get the "raw" data link
                for raw_link in soup2.find_all(id="raw-url"):
                    # get the raw data
                    rawtext = urlopen(urljoin(url, raw_link.get('href'))).read().decode()

                    # save it in the database
                    cursor.execute('INSERT OR IGNORE INTO transcripts (pk, transcript) VALUES (?, ?)',
                                   (title, rawtext,))


def sentiment_analysis():
    """
    Don't run these tests automatically, they take a long time.
    # >>> sentiment_analysis()
    {'neg': 1.0, 'neu': 0.0, 'pos': 0.0, 'compound': -0.0772} sorry?
    {'neg': 0.714, 'neu': 0.286, 'pos': 0.0, 'compound': -0.3612} It's ridiculous.
    {'neg': 0.714, 'neu': 0.286, 'pos': 0.0, 'compound': -0.3612} Lazy import.
    """
    from nltk.sentiment.vader import SentimentIntensityAnalyzer

    sid = SentimentIntensityAnalyzer()

    # Get the database
    connection = apsw.Connection("mikeckennedy.sqlite")
    cursor = connection.cursor()

    rows = list(cursor.execute("select pk, transcript from transcripts order by pk LIMIT 5, 10"))

    for row in rows:
        s = nltk.sent_tokenize(str(row[1]))

        for sentence in s:

            ps = sid.polarity_scores(sentence)
            # Only look at negative polarity scores
            if sid.polarity_scores(sentence)['compound'] != 0 and sid.polarity_scores(sentence)['neg'] >= 0.6:
                print(f'{ps} {sentence}')
