"""
In Python, inspecting a string for any particular property is usually a very simple task.
"""


def is_numerical(sentence: str) -> bool:
    """
    Well, Python's String object has the built-in method isnumeric() which returns True if so...
    >>> is_numerical('12345')
    True
    >>> is_numerical('0xabcdef')
    False
    >>> is_numerical('abcde')
    False
    >>> is_numerical('123abc')
    False
    >>> is_numerical('NaN')
    False
    """
    return sentence.isnumeric()


def is_number(sentence: str) -> bool:
    """
    If the conversion to a float worked, then the sentence was a number.
    >>> is_number('12345')
    True
    >>> is_number('0xabcdef')
    False
    >>> is_number('abcde')
    False
    >>> is_number('123abc')
    False
    >>> is_number('NaN')
    True
    >>> is_number('False')
    False
    """
    try:
        float(sentence)
        return True
    except ValueError:
        return False
