"""
How do you find the sum of two linked lists using Stack?

In Python, we'll use the collections.deque object - pronounced "deck".
This is a stack data structure but is double ended...
"""

from . import linkedlist as ll


def sum_lists(list1: ll.Node, list2: ll.Node) -> int:
    """
    This method receives two linked lists, sums all of the payload values together.
    We'll use a deck to store the values in each list.

    >>> n1: ll.Node = ll.Node(1)
    >>> n2: ll.Node = ll.Node(2)
    >>> n3: ll.Node = ll.Node(3)
    >>> n4: ll.Node = ll.Node(4)

    >>> n1.next = n2
    >>> n3.next = n4

    >>> sum_lists(n1, n3)
    10
    """
    sum1: int = 0

    from collections import deque
    d1 = deque()

    # Iterate over list 1 and append the payload onto the deque
    while list1:
        d1.append(int(list1.payload))
        list1 = list1.next

    # Iterate over list 2 and append the payload onto the deque
    while list2:
        d1.append(int(list2.payload))
        list2 = list2.next

    # Iterate over the deque, popping values from the left into the sum1 variable
    while True:
        try:
            sum1 += d1.pop()
        except IndexError:
            break

    return sum1
