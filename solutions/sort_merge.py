"""
The merge sort algorithm is nice!

This makes heavy use of recursion.
The goal is to divide and conquer (by continually dividing)
Finally, the halves are merged in order.
"""


def _merge(first_half: list, second_half: list, merged: list) -> list:
    """Merge two sorted Python lists S1 and S2 into properly sized list S."""
    i = j = 0
    while i + j < len(merged):
        if j == len(second_half) or (i < len(first_half) and first_half[i] < second_half[j]):
            merged[i + j] = first_half[i]  # recurse
            i += 1
        else:
            merged[i + j] = second_half[j]  # recurse
            j += 1
    return merged


def merge_sort(l: list) -> list:
    """
    Sort the elements of Python list S using the merge-sort algorithm.
    >>> merge_sort([10, 5, 100, 300, 1, 60])
    [1, 5, 10, 60, 100, 300]
    """
    n = len(l)
    if n < 2:
        return l  # list is already sorted

    # divide
    mid = n // 2
    first_half: list = l[0:mid]  # copy of first half
    second_half: list = l[mid:n]  # copy of second half

    # conquer (with recursion)
    first_half = merge_sort(first_half)  # recursively sort
    second_half = merge_sort(second_half)  # recursively sort

    # merge results
    return _merge(first_half, second_half, l)  # merge sorted halves back into a list
