"""
Decorators are used as functional annotations to methods, adding functionality encapsulated in a central
location. As such, they are widely used in many kinds of programs. The basics of a decorator is to define
an enclosure, itself taking as a parameter the function being wrapped.

The actual enclosure receives the parameters passed to the function being wrapped. Essentially, the original
function is no longer being called directly. Instead, the decorator intercepts the call, wrapping it with
generic functionality.


"""


def simple_decorator(function_being_wrapped):
    from functools import wraps
    @wraps(function_being_wrapped)
    def decorator_callback(*args, **kwargs):
        print(
            "Before the function call." + function_being_wrapped.__name__)  # we haven't yet called the function being wrapped.
        result = function_being_wrapped(*args, **kwargs)  # now we'll call, and pass through whatever parameters we got
        print("Finished the function wrapping.")
        return result

    return decorator_callback


@simple_decorator
def some_method(value: str) -> str:
    """
    This simple method is wrapped by the simple_decorator.

    :param value:
    :return:

    >>> some_method("anything")
    Before the function call.some_method
    Finished the function wrapping.
    'anything'
    """
    return value


def main():
    if not __debug__:
        raise RuntimeError("Can not execute in optimized python mode, the assertions have been removed.")
    else:
        assert __name__ == "__main__"

    print(some_method(1))


if __name__ == "__main__":
    main()
