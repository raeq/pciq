"""
This example build on what we learned about coroutines.
It will take a text string and label each word according to it's part of speech (noun, verb, adjective).

If we find a verb, we'll reverse it. If we found a noun, we'll capitalize it.
We'll use Python's Natural Language Toolkit to do this.

If you haven't yet installed NLTK:

Terminal:
pip3 install nltk --user

In the python console install wordnet:
> > > import nltk
> > > nltk.download("wordnet")
> > > nltk.download()

"""
import nltk
from nltk.corpus import wordnet as wn


def tokenize_sentence(sentence: str):
    """
    Ask NLTK to tokenize a sentence:
    >>> tokens = tokenize_sentence("The quick brown fox jumped over the lazy dog.")
    >>> print (tokens)
    The warm Brown Fox jump over the work-shy dog-iron.
    """
    tokens = nltk.pos_tag(nltk.word_tokenize(sentence))
    retval: str = ""
    for tag, v in tokens:
        if str(v) == 'NN':
            synset = wn.synsets(tag, pos=wn.NOUN)[-1]
            l = synset.lemmas()[-1].name()
            retval += str(l) + " "
        elif str(v) == 'JJ':
            synset = wn.synsets(tag, pos=wn.ADJ)[-1]
            l = synset.lemmas()[-1].name()
            retval += str(l) + " "
        elif str(v) == 'VBD':
            synset = wn.synsets(tag, pos=wn.VERB)[-1]
            l = synset.lemmas()[-1].name()
            retval += str(l) + " "
        elif str(v) == '.':
            retval = retval.rstrip() + tag
        else:
            retval += tag + " "

    # .lemmas()[0].antonyms()[0]
    return retval
