"""
Going from PIP to POP using WordNet
"""

from nltk.corpus import wordnet as wn

from .bst import Tree


def all_words(myword: str):
    """
    """

    t: Tree = Tree()

    for l in (lemma for lemma in wn.all_lemma_names() if len(lemma) == len(myword)):
        t.insert(l)

    print('a')
