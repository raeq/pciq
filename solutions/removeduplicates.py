"""
Removing duplicates from a list is relatively simple: use a set because a set only holds unique values.
"""


def remove_duplicates(numbers: list)-> list:
    """
    >>> remove_duplicates([1,2,2,3,3,3,4,5,2,2,3,3])
    [1, 2, 3, 4, 5]

    :param numbers:
    :return:
    """

    return list(set(sorted(numbers)))
