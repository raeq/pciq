"""
How do you swap two values without using a temporary variable?
"""


def swap_standard(a: int, b: int) -> tuple:
    """
    This is the traditional way of doing it, using a temporary variable,
    :param a: left value
    :param b: right value
    :return: a tuple of left and right, swapped

    >>> swap_standard(1, 2)
    (2, 1)

    """

    temp: int = a
    a = b
    b = temp

    return a, b


def swap_pythonic(a: int, b: int) -> tuple:
    """
    This is the pythonic way of doing this, using in-place assignment.

    :param a: left value
    :param b: right value
    :return: a tuple of left and right, swapped

    >>> swap_pythonic(1, 2)
    (2, 1)

    """

    a, b = b, a
    return a, b


def swap_arithmetic(a: int, b: int) -> tuple:
    """
    In most cases, the interviewer would be looking for the following solution.
    It uses arithmetic.

    :param a: left value
    :param b: right value
    :return: a tuple of left and right, swapped

    >>> swap_arithmetic(10, 15)
    (15, 10)
    """

    a = a + b  # a becomes 25
    b = a - b  # b becomes 10
    a = a - b  # a becomes 15

    return a, b


def swap_bitwise(a: int, b: int) -> tuple:
    """
    The following code uses bitwise operators on integers only!

    :param a: left value
    :param b: right value
    :return: a tuple of left and right, swapped

    >>> swap_bitwise(1, 3)
    (3, 1)
    """

    a = a ^ b  # 001 XOR 011 a becomes 010 (2)
    b = a ^ b  # 010 XOR 011 b becomes 001 (1)
    a = a ^ b  # 110 XOR 001 a becomes 011 (3)

    return a, b
