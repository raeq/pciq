"""
Assertions are generally used during the development phase of a program.
They stop execution when a certain condition is met. This is not generally the behaviour you would expect in a finished
piece of software, where it is likely more important to recover from an error condition.

The python interpreter can be invoked with the -O flag to enable optimizations. This only does two things:
    1. Removes assertions from the code.
    2. Changes the value of the global __debug__ variable from true to false.
"""


def assert1(value: str):
    """
    In this case we just want to make sure that the assert1 method is not called with value "Goodbye!".
    >>> assert1("Goodbye!")
    Traceback (most recent call last):
    ...
    AssertionError
    >>> assert1('Hello!')

    """
    assert value != 'Goodbye!'


def assert2(value: str):
    """
    We can also use assertions to check the type of parameter variables given to the method.
    Note that this checking is removed if run with -O, so we add a guard for that.

    >>> assert2('Hello!')   # This is a string value being passed, and should not raise an error.
    >>> assert2(1)          # This is not a string and should raise an assertion error
    Traceback (most recent call last):
    ...
    AssertionError
    """
    if not __debug__:
        raise RuntimeError("Not running in debug mode, assertions disabled.")
    assert isinstance(value, str)


def assert3(leftside: set, rightside: set):
    """
    In some pytest cases, the error produced by a failing assertion adds rich detail to the actual problem
    encountered during the test. Here an example using sets. We can also add an optional error message.

    >>>
    >>> left = set("123")
    >>> right = set("234")
    >>> assert3(left, right)    # Adding an optional message.
    Traceback (most recent call last):
    ...
    AssertionError: The two sets are not equivalent.
    """
    if not __debug__:
        raise RuntimeError("Not running in debug mode, assertions disabled.")
    assert leftside == rightside, "The two sets are not equivalent."


def assert55(value: bool):
    """
    It is easy to think you need to add parentheses to an assert call, but beware, this will always be interpreted as
    true!

    >>> assert55(False)

    #Traceback (most recent call last):
    #...
    #AssertionError: Parameter is not True
    """

    # pylint: disable=assert-on-tuple
    if not __debug__:
        raise RuntimeError("Not running in debug mode, assertions disabled.")
    assert (value is True, "Parameter is not True")  # This always evaluates to true!
