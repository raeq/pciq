import pytest

from solutions import algorithm_celebrity


@pytest.fixture(scope='function')
def thegraph():
    graph = algorithm_celebrity.get_random_graph(50)
    crowd = algorithm_celebrity.make_celebrity_in_crowd(graph)
    return crowd


def test_find_celebrity(thegraph):
    celeb = algorithm_celebrity.find_celebrity(thegraph)
    assert celeb is not None


def test_find_celebrity_bruteforce(thegraph):
    celeb = algorithm_celebrity.find_celebrity_bruteforce(thegraph)
    assert celeb is not None
