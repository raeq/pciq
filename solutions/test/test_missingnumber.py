import pytest

from solutions import missingnumber


@pytest.fixture(scope="module")
def positive_tests():
    """
    Tests to ensure known good values are returned.
    """
    return [1, 2, 5, 6, 7, 9]


def test_missing_number_comprehension_bruteforce_01(positive_tests):
    assert missingnumber.missing_number_comprehension_bruteforce(positive_tests) == [3, 4, 8]


def test_missing_numbers_naieve_01(positive_tests):
    assert missingnumber.missing_numbers_naieve([1, 3]) == 2


def test_missing_numbers_set_difference_01(positive_tests):
    assert missingnumber.missing_numbers_set_difference(positive_tests) == [3, 4, 8]


def test_missing_numbers_iterative_01(positive_tests):
    assert missingnumber.missing_numbers_iterative(positive_tests) == [3, 4, 8]


def test_missing_number_recursion_bruteforce_01(positive_tests):
    assert list(missingnumber.missing_number_recursion_bruteforce(positive_tests,
                                                             0,
                                                             len(positive_tests)-1)) == [3, 4, 8]


@pytest.fixture(scope="module")
def negative_tests():
    """
    Tests to ensure that incorrect results are not given.
    """
    return [1, 2, 5, 6, 7, 9, 11]


def test_missing_number_comprehension_bruteforce_02(negative_tests):
    assert missingnumber.missing_number_comprehension_bruteforce(negative_tests) != [3, 4, 8]


def test_missing_numbers_naieve_02(negative_tests):
    assert missingnumber.missing_numbers_naieve([1, 3]) != 3


def test_missing_numbers_set_difference_02(negative_tests):
    assert missingnumber.missing_numbers_set_difference(negative_tests) != [3, 4, 8]


def test_missing_numbers_iterative_02(negative_tests):
    assert missingnumber.missing_numbers_iterative(negative_tests) != [3, 4, 8]


def test_missing_number_recursion_bruteforce_02(negative_tests):
    assert missingnumber.missing_number_recursion_bruteforce(negative_tests,
                                                             negative_tests[0],
                                                             negative_tests[-1]) != [3, 4, 8]
