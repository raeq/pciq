"""
This is an implementation of a binary search tree.
It's very simple, it doesn't contain references to the parent so that several different
search algorithms aren't possible.
"""


class Node:
    """
    This represents a node in the binary tree.
    As in other areas of this project, you can see that I like to use the slots feature
    for memory efficient use of instance variables.
    """
    __slots__ = ['data', 'left', 'right']

    def __init__(self, value: object):
        self.left: Node = None
        self.right: Node = None
        self.data: object = value

    def __repr__(self):
        return f"Tree node with data value of '{self.data}'."


class Tree:
    """
    This is the tree class for managing the nodes.
    The insert function adds a value, and its parent node.


    """

    def __init__(self):
        self.root = None

    def insert(self, value: object, parent: Node = None):
        """
        Insert a node.
        If parent is not given, the root is assumed.
        If the root is not yet present, it will be created.
        Otherwise insert the node under the given parent.
        """
        if parent is None:
            if self.root is None:
                self.root = Node(value)  # Well, let's create a root node for this value.
                return  # and we're done here
            else:
                parent = self.root  # let's assume to create a new node somewhere under the parent.

        if value < parent.data:
            if parent.left is None:
                parent.left = Node(value)
            else:
                self.insert(value, parent.left)  # Use recursion to add a new leaf at the bottom left
        else:
            if parent.right is None:
                parent.right = Node(value)
            else:
                self.insert(value, parent.right)  # use recursion to add a new leaf at the bottom right

    def find(self, value):
        """
        Find a node with the given value
        """
        if self.root is not None:
            return self._find(value, self.root)
        else:
            return None

    def _find(self, val, node):
        """
        An efficient find method using less than / greater than to use the
        benefit of the binary search tree.
        """
        if val is node.data:
            return node
        elif val < node.data and node.left is not None:
            return self._find(val, node.left)
        elif val > node.data and node.right is not None:
            return self._find(val, node.right)

    def traversal_post_order(self, node: Node = None):
        """
        In post order traversal, we travel left, right, root
        """
        if node is not None:
            self.traversal_post_order(node.left)
            self.traversal_post_order(node.right)
            print(node)

    def traversal_in_order(self, node: Node = None):
        """
        In in order traversal, we travel right, then left, then root
        """
        if node is not None:
            self.traversal_in_order(node.left)
            print(node)
            self.traversal_in_order(node.right)

    def traversal_pre_order(self, node: Node = None):
        """
        In pre order traversal, we travel root, then left, then right
        """
        if node is not None:
            print(node)
            self.traversal_pre_order(node.left)
            self.traversal_pre_order(node.right)


def test_find():
    """
    >>> t: Tree = Tree()
    >>> t.insert(1000)
    >>> t.insert(2000, t.root)
    >>> t.insert(1000, t.root)
    >>> t.insert(80, t.root)
    >>> t.insert(40, t.root)
    >>> print(t.find(50))
    None
    >>> print(t.find(40))
    Tree node with data value of '40'.
    """


def test_post_order():
    """
    >>> t: Tree = Tree()
    >>> t.insert(10)
    >>> t.insert(7)
    >>> t.insert(5)
    >>> t.insert(16)
    >>> t.insert(8)
    >>> t.insert(14)
    >>> t.insert(20)
    >>> t.traversal_post_order(t.root)
    Tree node with data value of '5'.
    Tree node with data value of '8'.
    Tree node with data value of '7'.
    Tree node with data value of '14'.
    Tree node with data value of '20'.
    Tree node with data value of '16'.
    Tree node with data value of '10'.
    """


def test_in_order():
    """
    >>> t: Tree = Tree()
    >>> t.insert(10)
    >>> t.insert(7)
    >>> t.insert(5)
    >>> t.insert(16)
    >>> t.insert(8)
    >>> t.insert(14)
    >>> t.insert(20)
    >>> t.traversal_in_order(t.root)
    Tree node with data value of '5'.
    Tree node with data value of '7'.
    Tree node with data value of '8'.
    Tree node with data value of '10'.
    Tree node with data value of '14'.
    Tree node with data value of '16'.
    Tree node with data value of '20'.
    """


def test_pre_order():
    """
    >>> t: Tree = Tree()
    >>> t.insert(10)
    >>> t.insert(7)
    >>> t.insert(5)
    >>> t.insert(16)
    >>> t.insert(8)
    >>> t.insert(14)
    >>> t.insert(20)
    >>> t.traversal_pre_order(t.root)
    Tree node with data value of '10'.
    Tree node with data value of '7'.
    Tree node with data value of '5'.
    Tree node with data value of '8'.
    Tree node with data value of '16'.
    Tree node with data value of '14'.
    Tree node with data value of '20'.
    """


def test_list():
    """
    This shows how to create a list of values, then insert each of these into a binary search tree.
    >>> test_list()
    Tree node with data value of '10'.
    Tree node with data value of '7'.
    Tree node with data value of '5'.
    Tree node with data value of '8'.
    Tree node with data value of '16'.
    Tree node with data value of '14'.
    Tree node with data value of '20'.
    """

    l: list = list([10, 7, 5, 16, 8, 14, 20])  # make a list of integers
    t: Tree = Tree()  # make a binary search tree

    for v in l:
        t.insert(v)  # iterate the list and insert a new node for each value

    t.traversal_pre_order(t.root)  # print out the pre-order traversal as before
