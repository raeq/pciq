"""
How to count the occurences of a given character in a string?
Fewer tasks are easier in python!
"""


def count_char_counter(sentence: str, character: str) -> int:
    """
    Here we'll just use the Counter class in the collections module.
    We instantiate a new Counter with the sentence, and return the count value at key[character]
    >>> count_char_counter ('aaaabbbccda','a')
    5
    """
    from collections import Counter
    return Counter(sentence)[character]


def count_char_count(sentence: str, character: str) -> int:
    """
    Sometimes the question may specify "only use built in functions".
    Here is one way to do that using the String object's count() method.
    >>> count_char_count ('aaaabbbccdaa','a')
    6
    """
    return sentence.count(character)


def count_char_split(sentence: str, character: str) -> int:
    """
    Here is another way to do that using the String object's split() method.
    >>> count_char_split ('aaaabbbccdaa','a')
    6
    """
    return len(sentence.split(character)) - 1


def count_char_generator(sentence: str, character: str) -> int:
    """
    And in those WTF? moments, when someone says you shouldn'y use any in built functions (y tho?) then you can use
    something like this generator function.
    >>> count_char_generator ('aaaabbbccdaa','a')
    6
    """

    gen = (x for x in sentence if x is character)

    count: int = 0
    for x in gen:
        count += 1
    return count
