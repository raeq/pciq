"""
How do you find all pairs of an integer array whose sum is equal to a given number?
"""


def find_pair_sum(numbers: list, total: int)->dict:
    """
    This is quite tricky at first glance, the key to understanding it lies in decomposing the comprehension:
        if x + y == total : only return a result if a pair match for the given target is found
        for x in numbers for y in numbers : this is two iterations, building a matrix
        (x, y) means we need two values in our iterations
        return list(set(( ... ))) : means we return a list, the elements of which are a unique set - no duplicates


    >>> find_pair_sum([1,5,3,3,4,2] + list(range (6, 1000)),6)
    [(5, 1), (3, 3), (1, 5), (4, 2), (2, 4)]

    :param numbers:
    :param total:
    :return:
    """

    # first sort the integer list, make sure we are not wasting tie testing values which can't possibly sum to
    # our target total
    sorted_numbers = sorted(numbers)
    filtered_numbers: list = [x for x in sorted_numbers if x <= total]

    return list(set((x, y) for x in filtered_numbers for y in filtered_numbers if x + y == total))


def find_pair_sum_sorted(numbers: list, total: int)->dict:
    """
    This is quite tricky at first glance, the key to understanding it lies in decomposing the comprehension:
        if x + y == total : only return a result if a pair match for the given target is found
        for x in numbers for y in numbers : this is two iterations, building a matrix
        (x, y) means we need two values in our iterations
        return list(set(( ... ))) : means we return a list, the elements of which are a unique set - no duplicates


    >>> find_pair_sum_sorted([1,5,3,3,4,2], 6)
    [(5, 1), (3, 3), (1, 5), (4, 2), (2, 4)]

    :param numbers:
    :param total:
    :return:
    """
    from sortedcontainers import SortedList

    # first sort the integer list, make sure we are not wasting tie testing values which can't possibly sum to
    # our target total

    sorted_numbers: SortedList = SortedList(numbers)
    filtered_numbers: SortedList = [x for x in sorted_numbers if x <= total]
    return list(set((x, y) for x in filtered_numbers for y in filtered_numbers if x + y == total))


def find_pair_sum_slow(numbers: list, total: int)->dict:
    """

    >>> find_pair_sum_slow([1,5,3,3,4,2,6,1,0] + list(range (6, 100000)),6)
    [(0, 6), (1, 5), (2, 4), (3, 3)]

    :param numbers:
    :param total:
    :return:
    """
    from sortedcontainers import SortedList

    sorted_numbers: SortedList = SortedList(numbers)
    filtered_numbers: SortedList = [x for x in sorted_numbers if x <= total]

    result: set = set() # use a set to prevent duplicate entries

    indexes = {}
    for i, x in enumerate(filtered_numbers):
        indexes[x] = indexes.get(x, [])+[i]

    for i, x in enumerate(set(filtered_numbers)):
        for j in indexes.get(total - x, []):
            if i < j: # we only need matches where the left side is smallest
                result.add((x, total-x))

    # don't return a set, cast it to a list
    return list(result)
