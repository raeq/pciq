"""
Finding all possible permutations is a fun exercise. The permutations of 'yes' are:
    ['yes', 'yse', 'eys', 'esy', 'sye', 'sey']
"""


def permutate(word: str) -> list:
    """
    There is a built-in library function in iertools which is the preferred solution.
    >>> print (permutate('yes'))
    ['esy', 'eys', 'sey', 'sye', 'yes', 'yse']
    >>> print (permutate('abcd'))
    ['abcd', 'abdc', 'acbd', 'acdb', 'adbc', 'adcb', 'bacd', 'badc', 'bcad', 'bcda', 'bdac', 'bdca', 'cabd', 'cadb', 'cbad', 'cbda', 'cdab', 'cdba', 'dabc', 'dacb', 'dbac', 'dbca', 'dcab', 'dcba']
    >>> print (permutate('madam'))
    ['aadmm', 'aamdm', 'aammd', 'adamm', 'admam', 'admma', 'amadm', 'amamd', 'amdam', 'amdma', 'ammad', 'ammda', 'daamm', 'damam', 'damma', 'dmaam', 'dmama', 'dmmaa', 'maadm', 'maamd', 'madam', 'madma', 'mamad', 'mamda', 'mdaam', 'mdama', 'mdmaa', 'mmaad', 'mmada', 'mmdaa']
    """
    from itertools import permutations

    perms = set([''.join(p) for p in permutations(word)])  # coerce into a set to remove duplicates
    return sorted(list(perms))


def permutate_sjt(word: str) -> list:
    """
    Use the Steinhaus-Johnson-Trotter algorithm to return all permutations of iterable.

    The algorithm is applied to a 1-based set of integers representing the indices
    of the given word, then a shallow copy of the word is mutated and returned
    for each successive permutation.

    >>> for t in permutate_sjt('yes'):
    ...     print (t)
    yes
    yse
    sye
    sey
    esy
    eys
    """
    # A shallow copy of 'iterable'. This is what is mutated and yielded for each perm.
    sequence = list(word)
    length = len(sequence)
    indices = range(1, length + 1)

    # The list of directed integers: [-1, 1], [-1, 2], ...
    state = [[-1, idx] for idx in indices]

    # Add sentinels at the beginning and end
    state = [[-1, length + 1]] + state + [[-1, length + 1]]

    # The first permutation is the sequence itself
    yield ''.join(sequence)

    mobile_index = mobile_direction = direction = value = None
    while True:
        # 1. Find the highest mobile
        mobile = -1
        for idx in indices:
            direction, value = state[idx]
            if value > mobile and value > state[idx + direction][1]:
                # value is mobile and greater than the previous mobile
                mobile = value
                mobile_index = idx
                mobile_direction = direction
                if mobile == length:
                    # no point in continuing as mobile is as large as it can be.
                    break
        if mobile == -1:
            break

        # 2. Swap the mobile with the element it 'sees'
        sees = mobile_index + mobile_direction
        # ... first update the state
        state[mobile_index], state[sees] = state[sees], state[mobile_index]
        # ... then update the sequence
        sequence[mobile_index - 1], sequence[sees - 1] = sequence[sees - 1], sequence[mobile_index - 1]

        # 3. Switch the direction of elements greater than mobile
        if mobile < length:
            for idx in indices:
                if state[idx][1] > mobile:
                    state[idx][0] = -state[idx][0]

        yield ''.join(sequence)


def permutate_fys(word: str) -> str:
    """
    The Fischer-Yates shuffle is another algorithm for performing permutations.
    It produces a random permutation order.

    It needs to be called several times to produce random permutations of the input.
    >>> assert len(permutate_fys('madam'))
    """
    import random

    word = list(word)
    shuffle_position = len(word)

    # We stop at 1 because anything * 0 is 0 and 0 is the first index in the list
    # so the final loop through would just cause the shuffle to place the first
    # element in... the first position, again.  This causes this shuffling
    # algorithm to run O(n-1) instead of O(n).
    while shuffle_position > 1:
        i = random.randint(0, shuffle_position - 1)
        # We are using the back of the list to store the already-shuffled-indice,
        # so we will subtract by one to make sure we don't overwrite/move
        # an already shuffled element.
        shuffle_position -= 1
        # Move item from i to the front-of-the-back-of-the-list.
        word[i], word[shuffle_position] = word[shuffle_position], word[i]
    return ''.join(word)


def permutate_heap(L: str, ) -> list:
    """
    This is Sedgewick's heap permutation algorithm.
    As you can see, it implements position shuffling, but in a controlled and not random fashion.

    >>> permutate_heap('yes')
    ['yes', 'eys', 'sye', 'yse', 'esy', 'sey']
    """

    N = len(L)
    idx = [0 for i in range(N)]
    result = [''.join(L)]
    i = 1
    while i < N:
        if idx[i] < i:
            L = list(L)
            swap = i % 2 * idx[i]
            L[swap], L[i] = L[i], L[swap]
            result.append(''.join(L))
            idx[i] += 1
            i = 1
        else:
            idx[i] = 0
            i += 1
    return result
