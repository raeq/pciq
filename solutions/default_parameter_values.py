""" What do these values print?


def extendList(val, list=[]):
    list.append(val)
    return list

list1 = extendList(10)
list2 = extendList(123,[])
list3 = extendList('a')

print ("list1 = %s" % list1)
print ("list2 = %s" % list2)
print ("list3 = %s" % list3)

"""


def extendList(val, list=[]):
    """
    >>> print (extendList(10))
    [10]
    >>> print (extendList(123,[]))
    [123]

    This call does not return the result you might expect. The value 10 from the previous call was retained.
    That's because a list object with global scope was created the first time the method
    was invoked without specifying a second parameter. The next time the method is called without a second parameter,
    that list still exists, and is passed in to the method.

    >>> print (extendList('a'))
    [10, 'a']
    >>> print (extendList('b'))
    [10, 'a', 'b']
    >>> print (extendList(456,[]))
    [456]
    >>> print (extendList('c'))
    [10, 'a', 'b', 'c']
    >>> print (extendList2('a', ['b']))
    ['b', 'a']
    """
    list.append(val)
    return list


def extendList2(val, l=None):
    """
    >>> print (extendList2(10))
    [10]
    >>> print (extendList2(123,[]))
    [123]

    This time, there was no global list created in the default of the second parameter.

    >>> print (extendList2('a'))
    ['a']
    >>> print (extendList2('a', ['b']))
    ['b', 'a']
    """
    if l is None:
        l = list()
    l.append(val)
    return l
