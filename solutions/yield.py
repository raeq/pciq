"""
The yield statement is usually used to define the release of one step in the generation of an iterable sequence.

When we use the "for n in list" construct, under the hood a "yield" statement is giving up the current item as it
walks the list. The next call of  "for n" will receive another yielded value, until there are non left.

Again, under the hood, when there are no further items to yield, a StopIteration is raised, which let's the for
method know to stop asking for more elements to be yielded up.
"""


# pylint: disable=inconsistent-return-statements
def generate_list():
    """
    This demonstrates how the yield method is a kind of return, but one which remembers
    where it last was whilst iterating through a list.
    In this way, the generate function here does not actually walk to the end of the list unless the
    caller requires it.

    This function only returns values divisible by 7.

    >>> i = 0
    >>> for val in generate_list():
    ...     i += 1
    ...     if i>4 : break      # Try to get 4 values from the generator
    ...     print(val)
    105
    112
    """
    the_list = range(101, 100000)  # generate a fairly large list
    for val in the_list:
        if val % 117 == 0:  # we can stop iterating at 117 or any other value we choose
            return val  # return stops the iterations
        if val % 7 == 0:  # we are only interested in multiples of 7
            yield val  # but only yield up one element at a time
