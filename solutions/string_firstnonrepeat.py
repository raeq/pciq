"""
Finding the first character which doesn't repeat in a string is a very common question.
Depending on how it is answered reveals a lot about the depth of the programmer's experience.

This kind of task during real world tasks, where the length of the strings can be huge!
"""

search_space = 'hhhhhwwwwwwwwwooooooppppppihhhhhhhhvmmmmo'


def find_first_non_repeater_fast(sspace: str) -> str:
    """
    This is a clever solution to the problem by Roman Fursenko.
    We traverse the string, and we continually remove characters from it.
    >>> find_first_non_repeater_fast(search_space)
    'i'
    """

    while sspace != "":
        slen0 = len(sspace)  # store the length of the search space locally
        ch = sspace[0]  # get the first character in the search space

        sspace = sspace.replace(ch, "")  # remove all occurrences of this character! reduces the space
        slen1 = len(sspace)  # how long is the string without those characters

        if slen1 == slen0 - 1:  # just a difference of 1? Then this character only occurred once!
            return ch
    else:
        return ''


if __name__ == '__main__':
    find_first_non_repeater_fast(search_space)
