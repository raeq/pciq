"""
Sometimes it's necessary to inspect a string, and print characters which occur multiple times.
This is particularly useful in some scientific domains during which sequences (genes, peptides, enzymes...) are
analyzed in bulk.
"""


def find_duplicates(content: str):
    """
    This approach uses the defaultdict from the collections module.
    We instantiate a defaultdict with the int type. Whenever we look for a key but it's not in the dictionary,
    that key is automatically created with a default value (in our case of type int) which is then 0.
    It makes it easier to increment by 1 even for the case that we haven't seen this character before.

    >>> find_duplicates('The quick brown fox jumped over the lazy fox')
           8
    e      4
    o      4
    h      2
    u      2
    r      2
    f      2
    x      2
    """

    import collections

    d = collections.defaultdict(int)
    for c in content:
        d[c] += 1  # add each letter (c) to the dict and increment it's count

    for c in sorted(d, key=d.get, reverse=True):
        if d[c] > 1:  # only find characters occurring more than once
            print('%s %6d' % (c, d[c]))


def find_duplicates_counter():
    """
    Let's get a large corpus and try it out now.
    This method uses the collections.Counter method, which is very fast especially for large strings.
    The Counter methods creates a dictionary, each key is the ASCII code for each character encountered.
    The key's value is the number of occurrences.

    >>> find_duplicates_counter()
    a 2083
    b 325
    c 428
    d 1216
    e 4094
    f 596
    g 473
    h 1897
    i 1189
    j 14
    k 271
    l 1057
    m 485
    n 1809
    o 1922
    p 408
    q 12
    r 1647
    s 1764
    t 1937
    u 597
    v 208
    w 804
    x 14
    y 870
    z 7
    """
    from collections import Counter
    import urllib.request
    url = 'https://sourcebooks.fordham.edu/source/chaucer-prol.txt'  # get some text. Y not Chaucer?
    content = urllib.request.urlopen(url).read(100_000)

    c = Counter(content)
    for ch in sorted(c.keys()):
        if ch > 96 < 123:  # let's restrict this output to ASCII lower letters only
            print(chr(ch), c[ch])

