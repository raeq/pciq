"""
This module gives answers to several bit operations!
"""


def bitwise_double(a: int) -> int:
    """
    In this example, we shift the bits one place to the left.

    In this example:
    6 (ob0110) becomes 12 (ob1100)

    >>> bitwise_double(6)
    12
    """

    return a << 1


def bitwise_doubling(multiplicand: int, multiplier: int) -> int:
    """
    In this example, we double a number x amount of times

    In this example:
    6 (ob0110) becomes 12 (ob1100000) = 96

    Let's double our initial value of 6, four times (12, 24, 48. 96
    >>> bitwise_doubling(6, 4)
    96
    """

    return multiplicand << multiplier


def bitwise_halving(a: int) -> int:
    """
    Halving an integer can be accomplished by shifting bits to the right.
    >>> bitwise_halving(24)
    12
    """

    return a >> 1


def bitwise_halving_repeated(a: int, halving: int) -> int:
    """
    In this example, we halve a number x amount of times

    In this example:
    96 (ob1100000) becomes 6 (ob0110)

    Let's halve our initial value of 96, four times (48, 24, 12, 6)
    >>> bitwise_halving_repeated(96, 4)
    6
    """

    return a >> halving


def bitwise_addition(a: int, b: int) -> int:
    """
    We can use bitwise operators and zero testing to implement addition.
    while a ≠ 0
        c ← b and a
        b ← b xor a
        left shift c by 1
        a ← c
    return b

    >>> bitwise_addition(50, 30)
    80

    >>> bitwise_addition(36, 2384)
    2420
    """

    c: int = 0

    while a > 0:
        c = b & a
        b = b ^ a
        c = c << 1
        a = c

    return b


def bitwise_multiplication(a: int, b: int) -> int:
    """
    We can use bitwise operators and zero testing to implement multiplication.
    c ← 0
    while b ≠ 0
        if (b and 1) ≠ 0
            c ← c + a
        left shift a by 1
        right shift b by 1
    return c

    >>> bitwise_multiplication(50, 30)
    1500

    >>> bitwise_multiplication(7, 5)
    35
    """

    c: int = 0

    while b != 0:
        if (b & 1) != 0:
            c = bitwise_addition(c, a)  # yes, multiplication is just repeated addition
        a = a << 1
        b = b >> 1

    return c


def bitwise_subtraction(minuend: int, subtrahend: int) -> int:
    """
    Bitwise subtraction is a little tricky.
    First we need to make the minuend negative using the ~ operator (find it's complement)
    Then we find the bitwise AND of the minuend and the subtrahend
    Then keep halving the subtrahend until it goes to 0

    >>> bitwise_subtraction (100, 60)
    40

    >>> bitwise_subtraction (29, 17)
    12

    """
    # Iterate till there
    # is no carry
    while subtrahend != 0:
        c = (~minuend) & subtrahend
        minuend = minuend ^ subtrahend
        subtrahend = c << 1

    return minuend


def bitwise_division(numerator: int, denominator: int) -> tuple:
    """
    The numerator is also known as the diviend, the number to be divided.
    The denominator is also known as the divisor, what you want to divide by,

    >>> bitwise_division(17, 5)
    (3, 2)

    >>> bitwise_division(1004, 5)
    (200, 4)

    """

    quotient: int = 0
    remainder: int = 0

    if denominator == 0:
        raise ZeroDivisionError

    for i in range(31, -1, -1):

        quotient = quotient << 1
        remainder = remainder << 1
        remainder = remainder | (numerator & (1 << i)) >> i

        if remainder >= denominator:
            remainder = bitwise_subtraction(remainder, denominator)
            quotient = quotient | 1

    return quotient, remainder
