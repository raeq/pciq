"""
All things anagrams!
Anagrams are sets of words which have the same letters but a different meaning, for example 'read' and 'dear'.
Anagrams are also found in chemistry where they are known as isomers, for example dimethyl-ether and ethanol both
have the formula C2H6O, but are very different.
"""


def is_anagram(word1: str, word2: str) -> bool:
    """
    This is about the simplest way to see if two strings are anagrams. Simply sort them both and then compare
    the sorted values.
    >>> assert is_anagram('dear','reada')
    Traceback (most recent call last):
    ...
    AssertionError

    >>> assert is_anagram('dear','read')
    >>> assert is_anagram('dear*','*read')
    """
    return sorted(word1) == sorted(word2)


# prime numbers are assigned to letters by frequency of occurrence in English
primes = {'e': 2, 't': 3, 'a': 5, 'o': 7, 'i': 11, 'n': 13, 's': 17, 'h': 19, 'r': 23, 'd': 29, 'l': 31, 'c': 37,
          'u': 41, 'm': 43, 'w': 47, 'f': 53, 'g': 59, 'y': 61, 'p': 67, 'b': 71, 'v': 73, 'k': 79, 'j': 83,
          'x': 89, 'q': 97, 'z': 101}


def get_prime(value):
    """
    This helper function just returns the integer value (prime number) for each character
    If the searched for character is not a letter, it returns 1
    >>> get_prime('e')
    2
    >>> get_prime('*')
    1
    """
    p = primes.get(value)
    if type(p) != int:
        p = 1
    return p


def word_to_integer(word: str) -> int:
    """
    This function will return a large number as a result of multiplying many primes together
    >>> assert word_to_integer('read'), word_to_integer('dear')
    >>> word_to_integer('read')
    6670
    """
    # use list comprehension to make a list of primes for each letter
    # in the word
    l_primes = [get_prime(letter) for letter in word.rstrip()]

    result = 1
    for p in l_primes:
        result = result * p

    return result
