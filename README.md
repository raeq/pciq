# Answering Coding Questions in Python

Using Python 3 to answer typical coding interview questions.

## Background

These questions are taken from many sources of inspiration on the web, such as:
 
 * [techbeamers.com](https://web.archive.org/web/20181027120837/https://www.techbeamers.com/python-interview-questions-programmers/)
 * [data-flair.training](https://web.archive.org/web/20181027120954/https://data-flair.training/blogs/python-programming-interview-questions/)
 * [codementor.io](https://web.archive.org/web/20181027120958/https://www.codementor.io/sheena/essential-python-interview-questions-du107ozr6)
 * [codingcompiler.com](https://web.archive.org/web/20181027120916/https://codingcompiler.com/python-coding-interview-questions-answers/)
 * [interviewcake.com](https://web.archive.org/web/20181027121009/https://www.interviewcake.com/python-interview-questions)
 * [edureka.co](https://web.archive.org/web/20181027121025/https://www.edureka.co/blog/interview-questions/python-interview-questions/)
 * [dzone.com](https://web.archive.org/web/20181027121441/https://dzone.com/articles/50-programming-interview-questions-for-java-and-py)


## Downloading / Installing

1\. Install Python 3 and Git. I use [homebrew](https://brew.sh/) on mac: 
```shell
brew install python3 git
```

2\. Clone the repository to your local computer.
```shell
git clone https://gitlab.com/raeq/pciq.git
```

3\. Install the required python packages.
```shell
pip3 install -r requirements.txt --user
```

4\. Install NLTK
```shell
python3 -m nltk.dowloader all
```

5\. Run the tests:
```shell
pytest -v --doctest-modules --junit-xml=test.xml
```

## Licensing.
You can see the [file LICENSE](LICENSE) for details. This work may not be modified or commercially distributed.
Think of it as a book, which costs nothing. You can read it all you like, but you can't sell it.
Or claim you wrote it. Etc.

## Contributing
I will review all pull requests.

## Sections

The questions are laid out according to themes.

### Lists Coding Interview Questions in Python

1. How do you find the missing number in a given integer list of 1 to 100? [solutions/missingnumber.py](solutions/missingnumber.py)
1. How do you find the duplicate number on a given integer list? [solutions/duplicatenumber.py](solutions/duplicatenumber.py)
1. How do you find the largest and smallest number in an unsorted integer list? [solutions/largestsmallest.py](solutions/largestsmallest.py)
1. How do you find all pairs of an integer list whose sum is equal to a given number? [solutions/findpairs.py](solutions/findpairsum.py)
1. How do you find duplicate numbers in an integer list if it contains multiple duplicates? [solutions/duplicatenumber.py](solutions/duplicatenumber.py)
1. How are duplicates removed from a given list in Python? [solution/removeduplicates.py](solutions/removeduplicates.py)
1. How is an integer list sorted in place using the quicksort algorithm? [solutions/quicksort.py](solutions/quicksort.py)

### Closures and nested functions and functional programming in python
1. Describe [lambda](solutions/lambdas.py) callables.
1. Explain how loops can be removed to move from deterministic to [functional](solutions/maps.py) programming style.
1. What is a [closure](solutions/closures.py)?
1. How are closures used to define [decorators](solutions/decorators.py)?

### Exceptions and Assertions
1. [Assertions](solutions/asserions.py) stop program execution in the event of an error condition.
1. [Exceptions](solutions/exceptions.py) raise an exception - which can be handled by the caller - in the event of an error condition.

### Generators, Iterators, Yield, CoRoutines
1. Using [yield](solutions/yield.py) with a generator
1. Simple [coroutines](solutions/coroutines.py) receive data and use the send method

### Algorithms
1. If there are 128 competitors in an [efficient chess competition](solutions/algorithm_completegraph.py), during which each competitor must play every other competitor once, 
how many games of chess are played?
1. If a celebrity visits a party where everyone knows the celebrity but nobody knows the celebrity, how would you 
[discover the celebrity](solutions/algorithm_celebrity.py)?
1. Many interviewers like to ask about how to calculate a [fibonacci](solutions/algorithms_fibonacci.py) series.

### LinkedLists Coding Interview Questions in Python

1. How do you travers a singly linked list from the head to the tail ([ll_walk.py](solutions/ll_walk.py))?
1. How do you find the middle element of a singly linked list in one pass? ([ll_middle.py](solutions/ll_middle.py))
1. How do you check if a given linked list contains a cycle? How do you find the starting node of the cycle? ()
1. How do you reverse a linked list? ([ll_reverse.py](solutions/ll_reverse.py))
1. How are duplicate nodes removed in an unsorted linked list? ([ll_duplicates.py](solutions/ll_duplicates.py))
1. How do you find the length of a singly linked list? ()
1. How do you find the third node from the end in a singly linked list? ()
1. How do you find the sum of two linked lists using Stack? ([ll_deque.py](solutions/ll_deque.py))


### String Coding Interview Questions in Python

1. How do you print duplicate characters from a string? ([string_find_duplicates.py](solutions/string_find_duplicates.py))
1. How do you check if two strings are anagrams of each other? ([string_anagrams.py](solutions/string_anagrams.py))
1. How do you print the first non-repeated character from a string? ([string_firstnonrepeat.py](solutions/string_firstnonrepeat.py))
1. How can a given string be reversed using recursion? ([string_reverse.py](solutions/string_reverse.py))
1. How do you check if a string contains only digits? ([string_numerical.py](solutions/string_numerical.py))
1. How do you count a number of vowels and consonants in a given string? ([string_vowels.py](solutions/string_vowels.py))
1. How do you count the occurrence of a given character in a string? ([string_count.py](solutions/string_count.py))
1. How do you find all permutations of a string? ([string_permutations.py](solutions/string_permutations.py))
1. How do you reverse words in a given sentence without using any library method? ([string_reversewordorder](solutions/string_reversewordorder.py))
1. How do you check if two strings are a rotation of each other? ([string_rotation.py](solutions/string_rotations.py))
1. How do you check if a given string is a palindrome? ([string_palindrome.py](solutions/string_palindrome.py))


### Binary Tree Coding Interview Questions in Python

1. How is a binary search tree implemented? ([bst.py](solutions/bst.py))
1. How do you perform preorder traversal in a given binary tree? ([bst.py](solutions/bst.py))
1. How do you perform an in-order traversal in a given binary tree? ([bst.py](solutions/bst.py))
1. How do you implement a post-order traversal algorithm? ([bst.py](solutions/bst.py))
1. How are all leaves of a binary search tree printed? ([bst.py](solutions/bst.py))
1. How do you count a number of leaf nodes in a given binary tree? ()
1. How do you perform a binary search in a given list? ([bst.py](solutions/bst.py))


### Miscellaneous Coding Interview Questions in Python

1. How is a bubble sort algorithm implemented? ([sort_bubble.py](solutions/sort_bubble.py))
1. How is an iterative quicksort algorithm implemented? ([quicksort.py](solutions/quicksort.py))
1. How do you implement an insertion sort algorithm? ([sort_insertion.py](solutions/sort_insertion.py))
1. How is a merge sort algorithm implemented? ([sort_merge.py](solutions/sort_merge.py))
1. How is a radix sort algorithm implemented? ([sort_radix.py](solutions/sort_radix.py))
1. How do you check if two rectangles overlap with each other? ([rectangle.py](solutions/rectangle.py))
1. How do you design a vending machine? ()
1. [FizzBuzz](solutions/fizzbuzz.py)
1. Defining default values in method definitions.

### Object Oriented Programming
1. How do you implment a system which outputs changes to the console every time a class instance property changes? ([oop_observer.py](solutions/oop_observer.py)

### Natural Language Processing
1. How do you get the synonyms of a given word? ([nltk_synonyms.py](solutions/nltk_synonyms.py))
1. How do you go from STACK to PLANT changing one letter at a time using only valid words?


### Bitwise Coding Interview Questions in Python
1. How do you swap two numbers without using the third variable? ([misc_inplace_swap.py](solutions/misc_inplace_swap.py))
1. How do you double a number without using the arithmetic operators like * or + ? ([misc_bitwise.py](solutions/misc_bitwise.py))
1. How do you halve a number without using the arithmetic operators like * or + ? ([misc_bitwise.py](solutions/misc_bitwise.py))
1. How do you multiply a number without using the arithmetic operators like * or + ? ([misc_bitwise.py](solutions/misc_bitwise.py))
1. How do you add two numbers without using the arithmetic operators like * or + ? ([misc_bitwise.py](solutions/misc_bitwise.py))
1. How do you subtract two numbers without using the arithmetic operators like * or + ? ([misc_bitwise.py](solutions/misc_bitwise.py))
1. How do you divide a number without using the arithmetic operators like * or + ? ([misc_bitwise.py](solutions/misc_bitwise.py))

### PyBites Code Challenges
58. Code Challenge 58 - Analyze Podcast Transcripts with NLTK - Part I ([pybites_58.py](solutions/pybites_58.py))

###  Web Service Questions
1. How do you consume a RESTful API? ([rest_client.py])